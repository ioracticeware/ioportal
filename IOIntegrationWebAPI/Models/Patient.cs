﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class PatientDetail
    {
        public string PatientId { get; set; }
        public string PatientDesc { get; set; }
    }

    public class Patient
    {
        public string PatientAccount { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }

        public Patient()
        {
           //Default Constructor
        }

        public Patient(string PatientAccount, string DecryptedPracticeId)
        {
            string _SelectQry = "Select LastName, FirstName, MiddleName, Cast(DOB as Date) as DOB, Gender From Medflow.PatientDemographicsInformation Where PatientAccount = '" + PatientAccount + "'";
            using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[DecryptedPracticeId].ToString()))
            {
                Connection.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, Connection);
                SqlDataReader objReader = cmd.ExecuteReader();
                if (objReader.Read())
                {
                    this.PatientAccount = PatientAccount;
                    this.FirstName = objReader["FirstName"].ToString();
                    this.MiddleName = objReader["MiddleName"].ToString();
                    this.DOB = objReader["DOB"].ToString();
                    this.LastName = objReader["LastName"].ToString();
                    this.Gender = objReader["Gender"].ToString();
                }
                objReader.Close();
                Connection.Close();
            }
        }

        public int AddPatientDetails(string DecryptedPracticeId)
        {
            int retStatus = 0;
            try
            {
                string _InsertQry = "Insert into Medflow.PatientDemographicsInformation(PatientAccount, LastName, FirstName, MiddleName, DOB, Gender) Values ('" + this.PatientAccount + "', '" + this.LastName + "', '" + this.FirstName + "', '" + this.MiddleName + "', '" + this.DOB + "', '" + this.Gender + "')";
                using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[DecryptedPracticeId].ToString()))
                {
                    Connection.Open();
                    SqlCommand cmd = new SqlCommand(_InsertQry, Connection);
                    retStatus = cmd.ExecuteNonQuery();
                    Connection.Close();
                }
            }
            catch (Exception ee)
            {
                retStatus = 0;
            }
            return retStatus;
        }
    }
}