﻿using IOIntegrationWebAPI.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class EmailNotificationEntity
    {
        public string ProviderId { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string Dated { get; set; }
        public string Status { get; set; }
    }

    public class EmailNotification
    {
        public ProviderDetials ProviderDetail = null;
        public string EmailAddress { get; set; }
        public string IsActive { get; set; }

        public EmailNotification(string providerId, string practiceId)
        {
            string _SelectQry = "Select EmailAddress From Medflow.User_AlertDetails UAD Where UAD.ProviderId = '" + providerId + "'";
            string EncryptedPracticeId = EncryptDecrypt.Encrypt(practiceId);
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[practiceId].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, con);
                SqlDataReader objRdr = cmd.ExecuteReader();
                if (objRdr.Read())
                {
                    this.EmailAddress = objRdr["EmailAddress"].ToString();
                    this.ProviderDetail = new ProviderDetials("1", providerId, EncryptedPracticeId);
                    this.IsActive = "1";
                    objRdr.Close();
                }
                con.Close();
            }
        }
    }


}