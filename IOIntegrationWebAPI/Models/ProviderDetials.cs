﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{    

    public class ProviderDetials
    {
        public string ProviderId { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IsUserActive { get; set; }
        public string UserEmail { get; set; }
        public string IsAccountActivated { get; set; }
        public string CreatedDate { get; set; }
        public string IsAdmin { get; set; }
        public string PracticeId { get; set; }

        public ProviderDetials()
        {

        }

        public ProviderDetials(string paramType, string paramValue, string PracticeId)
        {
            string _SelectQry = "";
            if (paramType == "1")
            {
                _SelectQry = "Select U.Id, U.FirstName, U.LastName, U.PId, U.DisplayName, UC.UserName, UC.Password, UC.IsUserActive, UC.UserEmail, UC.IsAccountActivated, UC.CreatedDate, U.isAdmin from medflow.Users U Inner Join Medflow.UserCredentials UC on U.Id = UC.ProviderId and UC.ProviderId = '" + paramValue + "'";
            }
            else
            {
                _SelectQry = "Select U.Id, U.FirstName, U.LastName, U.PId, U.DisplayName, UC.UserName, UC.Password, UC.IsUserActive, UC.UserEmail, UC.IsAccountActivated, UC.CreatedDate, U.isAdmin from medflow.Users U Inner Join Medflow.UserCredentials UC on U.Id = UC.ProviderId and UC.UserEmail = '" + paramValue + "'";
            }
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(PracticeId);
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[DecryptedPracticeId].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, con);
                SqlDataReader objRdr = cmd.ExecuteReader();
                if (objRdr.Read())
                {
                    this.ProviderId =  objRdr["Id"].ToString();
                    this.DisplayName = objRdr["DisplayName"].ToString();
                    this.FirstName = objRdr["FirstName"].ToString();
                    this.LastName = objRdr["LastName"].ToString();
                    this.PID = objRdr["PID"].ToString();
                    this.UserName = objRdr["UserName"].ToString();
                    this.Password = objRdr["Password"].ToString();
                    this.IsUserActive = objRdr["IsUserActive"].ToString();
                    this.UserEmail = objRdr["UserEmail"].ToString();
                    this.IsAccountActivated = objRdr["IsAccountActivated"].ToString();
                    this.CreatedDate = objRdr["CreatedDate"].ToString();
                    this.IsAdmin = objRdr["IsAdmin"].ToString();
                    this.PracticeId = PracticeId;
                    objRdr.Close();
                }
                con.Close();
            }
        }

    }
}