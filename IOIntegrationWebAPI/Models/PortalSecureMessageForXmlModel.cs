﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Xml.Serialization;
using IO.Practiceware.Integration.Omedix;
using Microsoft.Win32;

namespace IOIntegrationWebAPI.Models
{

    [XmlRoot("Xml")]
    public class RequestDoctorResponseModel
    {
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }

        public PatientModel Patient { get; set; }
        public PortalSecureMessageModel SecureMessage { get; set; }
    }


    public class PatientModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int PatientAccount { get; set; }
        [XmlElement(ElementName = "DOB")]
        public string DOB { get; set; }
        public string Gender { get; set; }
    }

    public class PortalSecureMessageModel
    {
        public string Message { get; set; }
        public int PortalMessageId { get; set; }
        public string RequestedDate { get; set; }
        public string ProviderId { get; set; }
        public string Subject { get; set; }
        public string Sender { get; set; }
        public int EMRMessageId { get; set; }

        // new property for globally access
        public int PortalInboxId { get; set; }

    }

    public class PatientPortalInbox
    {
        public string ProviderId { get; set; }
        public string Subject { get; set; }
        public int PatientId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ExternalId { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string IsRead { get; set; }
    }

    public class PatientAmendmentDetail
    {
        public string PatientId { get; set; }
        public string ExternalId { get; set; }
        public string Message { get; set; }
        public string RequestedDate { get; set; }
        public string Sender { get; set; }
        public string ProviderId { get; set; }
    }

    public class Validate
    {
        public int UserExists { get; set; }
        public string id { get; set; }
        public string DisplayName { get; set; }
        public string PracticeName { get; set; }
        public string PracticeId { get; set; }
        public string isAdmin { get; set; }
        public string isAccountActivated { get; set; }
    }

    public class PasswordStatus
    {
        public string ResetStatus { get; set; }
    }

    public class PortalInboxHistory
    {
        public int MessageId { get; set; }
        public string Description { get; set; }
        public string FromPatient { get; set; }
        public string isClosed { get; set; }
    }

    public class SendReply
    {
        public int Portalinboxid { get; set; }
        public string Description { get; set; }
        public int status { get; set; }
    }

    public class MessageCount
    {
        public int Inbox { get; set; }
        public int Outbox { get; set; }
        public int Closed { get; set; }
    }


    public class Providerinfo
    {
        public string id { get; set; }
        public string displayname { get; set; }
        public string firstname { get; set; }
        public string LastName { get; set; }

    }

    public class CloseMessage
    {
        public string closed { get; set; }
    }

    public class UserProfile
    {
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PID { get; set; }
        public string PracticeName { get; set; }
        public string isAdmin { get; set; }

    }


    //public class PatientModel
    //{
    //    public int PatientId { get; set; }
    //    public string LastName { get; set; }
    //    public string FirstName { get; set; }
    //    public string MiddleName { get; set; }
    //    public DateTime DateOfBirth { get; set; }
    //    public string Gender { get; set; }
    //}
}