﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class AmendmentsStats
    {
        public int NewRequest { get; set; }
        public int PendingRequest { get; set; }
        public int CloseRequest { get; set; }
    }

    public class AmendmentRequest
    {
        public string RequestId { get; set; }
        public string PatientId { get; set; }
        public string PatientDOB { get; set; }
        public string PatientGender { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientMiddleName { get; set; }
        public string PatientLastName { get; set; }
        public string ProviderId { get; set; }
        public string ExternalId { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public DateTime RequestedDate { get; set; }
        public string ResponseStatus { get; set; }
        public DateTime ResponseDateTime { get; set; } 
    }

    public class AmendmentResponseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string MiddleName { get; set; }
        public int PatientAccount { get; set; }
        public string ExternalId { get; set; }
        public string Message { get; set; }
        public string Response { get; set; }
        public string ResponseDate { get; set; }
        public string ProviderId { get; set; }
    }

    public class AmendmentDetail
    {
        public static string FindMessage(string RequestId, string PracticeId)
        {
            string retMessage = "";
            string _SelectQry = "SELECT PAI.Message From Medflow.PatientAmendmentInfo PAI Where PAI.Id = " + RequestId;
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[PracticeId].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, con);
                SqlDataReader objRdr = cmd.ExecuteReader();
                if (objRdr.Read())
                {
                    retMessage = objRdr["Message"].ToString().Replace("'", "''");
                }
                objRdr.Close();
            }
            return retMessage;
        }
    }


}