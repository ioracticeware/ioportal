﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class UserCredentials
    {
        public string ProviderId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserEmail { get; set; }
        public string isAccountActivated { get; set; }

        public UserCredentials()
        {
           
        }

        public UserCredentials(string ProviderId, string PracticeKey)
        {
            string _SelectQry = "Select * From Medflow.UserCredentials Where ProviderId = '" + ProviderId + "'";
            using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[PracticeKey].ToString()))
            {
                Connection.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, Connection);
                SqlDataReader objReader = cmd.ExecuteReader();
                if (objReader.Read())
                {
                    this.UserName = objReader["UserName"].ToString();
                    this.Password = objReader["Password"].ToString();
                    this.UserEmail = objReader["UserEmail"].ToString();
                    this.isAccountActivated = objReader["isAccountActivated"].ToString();
                }
                else
                {
                    this.UserName = "";
                }
                objReader.Close();
                Connection.Close();
            }
        }

    }

    public class Users
    {
        public string ProviderId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string PracticeId { get; set; }
        public string PracticeName { get; set; }
        public string Pin { get; set; }
        public string IsAdmin { get; set; }
        public UserCredentials Crdentials = null;


        public Users()
        {
            // Default Construtor
        }

        public Users(string ProviderId, string practiceKey)
        {
            string _SelectQry = "Select * From Medflow.Users Where Id = '" + ProviderId + "'";
            using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[practiceKey].ToString()))
            {
                Connection.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, Connection);
                SqlDataReader objReader = cmd.ExecuteReader();
                if (objReader.Read())
                {
                    this.ProviderId = objReader["Id"].ToString();
                    this.FirstName = objReader["FirstName"].ToString();
                    this.LastName = objReader["LastName"].ToString();
                    this.DisplayName = objReader["DisplayName"].ToString();
                    this.PracticeId = objReader["PracticeId"].ToString();
                    this.PracticeName = objReader["PracticeName"].ToString();
                    this.Pin = objReader["PID"].ToString();
                    this.IsAdmin = objReader["IsAdmin"].ToString();
                    this.Crdentials = new UserCredentials(this.ProviderId, this.PracticeId);
                }
                else
                {
                    this.ProviderId = "";
                }
                objReader.Close();
                Connection.Close();
            }
        }

        public UserCredentials RegisterNewUser()
        {
            UserCredentials objUC = null;
            string _insertUserQry = "Insert into Medflow.Users(Id, FirstName, LastName, DisplayName, PracticeId, PracticeName, PID, IsAdmin) Values ('" + this.ProviderId + "', '" + this.FirstName.Replace("'", "''") + "',  '" + this.LastName.Replace("'", "''") + "', '" + this.DisplayName.Replace("'", "''") + "', '" + this.PracticeId + "', '" + this.PracticeName.Replace("'", "''") + "', '" + this.Pin + "', '" + this.IsAdmin + "');";
            using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[this.PracticeId].ToString()))
            {
                Connection.Open();
                SqlCommand cmd = new SqlCommand(_insertUserQry, Connection);
                if (cmd.ExecuteNonQuery() == 1)
                {
                    string _insertUserCredentialQry = "Insert into Medflow.UserCredentials(ProviderId, UserName, Password, IsUserActive, IsAccountActivated, CreatedDate) Select a.Id, Substring(FirstName,1,1) + LastName, SUBSTRING(Cast(NEWID() as nvarchar(Max)), 1, 6), 1, 0, GETDATE() From Medflow.Users a Where a.Id = '" + this.ProviderId + "' and a.Id not in (Select providerid From Medflow.UserCredentials)";
                    cmd = new SqlCommand(_insertUserCredentialQry, Connection);
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        objUC = new UserCredentials(this.ProviderId, this.PracticeId);
                    }
                }
            }
            return objUC;
        }

        public UserCredentials RegisterUserCredentials()
        {
            UserCredentials objUC = null;
            using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[this.PracticeId].ToString()))
            {
                Connection.Open();
                string _insertUserCredentialQry = "Insert into Medflow.UserCredentials(ProviderId, UserName, Password, IsUserActive, IsAccountActivated, CreatedDate) Select a.Id, Substring(FirstName,1,1) + LastName, SUBSTRING(Cast(NEWID() as nvarchar(Max)), 1, 6), 1, 0, GETDATE() From Medflow.Users a Where a.Id = '" + this.ProviderId + "' and a.Id not in (Select providerid From Medflow.UserCredentials)";

                SqlCommand cmd = new SqlCommand(_insertUserCredentialQry, Connection);
                if (cmd.ExecuteNonQuery() == 1)
                {
                    objUC = new UserCredentials(this.ProviderId, this.PracticeId);
                }
            }
            return objUC;
        }

    }
}