﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class LoginViewModel
    {
        public string PersonalId { get; set; }
        public string PracticeId { get; set; }
    }
}