﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class OcularMedicationResponseModel
    {
        public Patient PatientDetail = null;
        public string ExternalId { get; set; }
        public string ProviderId { get; set; }
        public string ReplyMessage { get; set; }
        public string ResponseDate { get; set; }
    }

    public class OcularMedicationDetail
    {
        public string RequestId { get; set; }
        public string PatientAccount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string ExternalId { get; set; }
        public string InstructionName { get; set; }
        public string MedicineName { get; set; }
        public string Description { get; set; }
        public string Sender { get; set; }
        public string ProviderId { get; set; }
        public DateTime RequestedDate { get; set; }
        public string IsRead { get; set; }
        public string RepliedMessage { get; set; }
        public DateTime ResponseDate { get; set; }
    }

    public class OcularMedicationStats
    {
        public int NewRequest { get; set; }
        public int PendingRequest { get; set; }
        public int CloseRequest { get; set; }
    }
}