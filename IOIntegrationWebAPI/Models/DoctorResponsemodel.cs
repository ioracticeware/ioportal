﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class DoctorResponsemodel
    {
        public string FirstName  { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int PatientAccount { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string Message { get; set; }
       
        public string Subject { get; set; }
        public string EMRMessageId { get; set; }
        public int PortalMessageId { get; set; }
        public int ProviderId { get; set; }
    }
}