﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class EncryptDecrypt
    {
        public static string Decrypt(string cipherText)
        {
            if (cipherText != null && cipherText != "")
            {
                int numberChars = cipherText.Length;
                byte[] bytes = new byte[numberChars / 2];
                for (int i = 0; i < numberChars; i += 2)
                {
                    bytes[i / 2] = Convert.ToByte(cipherText.Substring(i, 2), 16);
                }
                return System.Text.Encoding.Unicode.GetString(bytes);
            }
            else
            {
                return "0";
            }
        }

        public static string Encrypt(string input)
        {
            if (input != null && input != "")
            {
                Byte[] stringBytes = System.Text.Encoding.Unicode.GetBytes(input);
                StringBuilder sbBytes = new StringBuilder(stringBytes.Length * 2);
                foreach (byte b in stringBytes)
                {
                    sbBytes.AppendFormat("{0:X2}", b);
                }
                return sbBytes.ToString();
            }
            else
            {
                return "0";
            }
        }
    }
}