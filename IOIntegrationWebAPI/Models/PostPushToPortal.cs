﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class PostPushToPortal
    {
        public string ClientKey { get; set; }
        public int ActionTypeLookupTypeId { get; set; } //ActionTypeLookupTypeId
        public string MessageData { get; set; }
        public DateTime ProcessedDate { get; set; }
        public int ExternalId { get; set; }
    }

    public class Parameters2
    {
        public int PatientId { get; set; }
        public int IsActive { get; set; }
    }

    public class PostPushToPortalParameters
    {

        public string ClientKey { get; set; }
        public int ActionTypeLookupTypeId { get; set; }
        public string MessageData { get; set; }
        public DateTime ProcessedDate { get; set; }
        public int ExternalId { get; set; }
        public int PatientId { get; set; }
        public int IsActive { get; set; }
    }
}