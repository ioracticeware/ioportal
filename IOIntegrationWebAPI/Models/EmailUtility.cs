﻿using IOIntegrationWebAPI.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class EmailUtility
    {
        MedflowPortalBusiness business = new MedflowPortalBusiness();
        private static int SendEmail(string To, string Body, string Subject)
        {
            int retStatus = 0;
            try
            {
                using (SmtpClient SmtpMail = new SmtpClient("smtp.elasticemail.com", 587))
                {
                    MailMessage message = new MailMessage("help@iopracticeware.com", To, Subject, Body);
                    message.From = new MailAddress("help@iopracticeware.com", "DONOTREPLY@iopracticeware.com");
                    message.Priority = MailPriority.Normal;
                    message.IsBodyHtml = true;
                    SmtpMail.Send(message);
                    retStatus = 1;
                }
            }
            catch (Exception ex)
            {
                MedflowPortalBusiness business = new MedflowPortalBusiness();
                business.LoggerExceptions("Email Sending Failure", ex.Message.ToString());
                retStatus = 0;
            }
            return retStatus;
        }


        // Mail For Forgot Password Details
        public static int ForgotPassword(ProviderDetials objProvider)
        {
            int retStatus = 0;
            string ResetPasswordLink = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"].ToString() + "Account/ResetPassword/" + objProvider.PracticeId + "/" + EncryptDecrypt.Encrypt(objProvider.ProviderId);
            string HTML_Template = "<html><head></head><body><div style='border:solid #f1f1f1 5px;padding:10px;'> <p>Hello " + objProvider.DisplayName + "</p> <p>Greetings!</p> <p>We have received a request from your end for a new password for IO secure Messaging Dashboard.</p> <p>Please click on below link to reset your password.</p> <p>" + ResetPasswordLink + "</p> <p>We will be here to help you with any step along the way. You can get in touch with us by emailing us at support@iopracticeware.com</p> <p>Regards,</p> <p>IO Practiceware Team</p> <p><img src='https://portal.iopracticeware.com/PatientDashBoradDemo/images/IOlogo_new.gif'></img></p><p>110 Wall Street, New York, 10005 (US)<br />(212-844-0105)<br /><br />New York</p> </div> </body> </html>";
            retStatus = EmailUtility.SendEmail(objProvider.UserEmail, HTML_Template, "IO Secure Messaging - Password Detail");
            return retStatus;
        }

        // Nottification Mail For Secure Message
        public static int SecureMessageEmailNotifction(EmailNotification objSMEN)
        {
            int retStatus = 0;
            string Link = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"].ToString() + "Account/Login/" + objSMEN.ProviderDetail.PracticeId + "/";
            string HTML_Template = "<html><head></head><body><div style='border:solid #f1f1f1 5px;padding:10px;'><p>Hello,</p><p>Greetings!</p><p>A new message has been sent to " + objSMEN.ProviderDetail.DisplayName + ".</p><p>Please click on below link to check that message.</p> <p>" + Link + "</p><p>We will be here to help you with any step along the way. You can get in touch with us by emailing us at support@iopracticeware.com</p> <p>Regards,</p> <p>IO Practiceware Team</p> <p><img src='https://portal.iopracticeware.com/PatientDashBoradDemo/images/IOlogo_new.gif'></img></p><p>110 Wall Street, New York, 10005 (US)<br />(212-844-0105)<br /><br />New York</p> </div> </body> </html>";
            retStatus = EmailUtility.SendEmail(objSMEN.EmailAddress, HTML_Template, "IO Secure Message Notifiction - New Message");
            return retStatus;
        }


    }
}