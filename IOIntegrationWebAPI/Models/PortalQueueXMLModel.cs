﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOIntegrationWebAPI.Models
{
    public class PortalQueueXMLModel
    {
        public string MessageData { get; set; }
        public int DatabaseId { get; set; }
        public int ActionTypeLookupTypeId { get; set; }
        public int PatientId { get; set; }
        public int ExternalId { get; set; }
        public string ClientKey { get; set; }
    }


    public class PatientDemographicsInformation
    {
        public int PatientAccount { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public DateTime Dob { get; set; }
        public string Gender { get; set; }
        public string ClientKey { get; set; }
    }
}