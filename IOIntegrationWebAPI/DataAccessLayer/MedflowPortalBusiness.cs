﻿using System.Linq;
using IO.Practiceware.Data;
using IOIntegrationWebAPI.Models;
using Soaf;
using Soaf.Data;
using System;
using System.Data;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Schema;



namespace IOIntegrationWebAPI.DataAccessLayer
{
    public class MedflowPortalBusiness
    {
        public async Task<int> SavePortalQueueXml(PortalQueueXMLModel model)
        {
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;
            try
            {
                using (var db = new MedflowContext(model.ClientKey))
                {
                    var query = String.Format("INSERT INTO medflow.PortalQueueXML(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, ExternalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", model.MessageData, model.ActionTypeLookupTypeId, DateTime.Now, model.PatientId, 1, model.ExternalId);

                    rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                }
            }

            catch (Exception ex)
            {
                business.LoggerExceptions("SavePortalQueueXml", "Error while inserting PortalQueueXMl Table -" + ex.Message);
            }
            return rowsaffected;
        }

        public async static Task SaveSecureMessageFromPortal(PortalQueueXMLModel model)
        {
            var business = new MedflowPortalBusiness();
            try
            {
                //model.MessageData = "<Xml><Type>RequestDoctorResponse</Type><Patient><FirstName>Gautami</FirstName><LastName>Chaluvadi</LastName><MiddleName></MiddleName><PatientAccount>63220</PatientAccount><DOB>06/06/2006</DOB><Gender>FEMALE</Gender></Patient><SecureMessage><Message>Testing</Message><PortalMessageId>1</PortalMessageId><RequestedDate>06/06/2006</RequestedDate><ProviderId>21</ProviderId><Subject>Testing</Subject><Sender>Patient</Sender></SecureMessage></Xml>";

                //<Xml><Type>RequestDoctorResponse</Type><Patient><FirstName>IOP</FirstName><LastName>Test</LastName><MiddleName>Portal</MiddleName><PatientAccount>1</PatientAccount><DOB>08/13/1989</DOB><Gender>MALE</Gender></Patient><SecureMessage><Message>Dear Portal Please Run</Message><PortalMessageId>1</PortalMessageId><RequestedDate>04/06/2016 12:50:33 </RequestedDate><ProviderId>2</ProviderId> <Subject>Billing</Subject><Sender>1</Sender></ SecureMessage ></Xml>;
                string xmlMesageData = model.MessageData;
                var xmlDoc = XDocument.Parse(xmlMesageData);

                var providerserializer = new XmlSerializer(typeof(RequestDoctorResponseModel));//PatientModelRequestDoctorResponseModel
                var msgModel = (from xmlrep in xmlDoc.Descendants("Xml")
                                select providerserializer.Deserialize(xmlrep.CreateReader()) as RequestDoctorResponseModel).SingleOrDefault(); //PatientModel

                business.LoggerExceptions("SaveSecureMessageFromPortal", "Patient Message XML parsing is Complete");

                Patient objPatient = new Patient(msgModel.Patient.PatientAccount.ToString(), model.ClientKey);
                if (objPatient.LastName == null)
                {
                    Patient objPTemp = new Patient()
                    {
                        DOB = msgModel.Patient.DOB,
                        PatientAccount = msgModel.Patient.PatientAccount.ToString(),
                        FirstName = msgModel.Patient.FirstName,
                        LastName = msgModel.Patient.LastName,
                        MiddleName = msgModel.Patient.MiddleName,
                        Gender = msgModel.Patient.Gender
                    };
                    objPTemp.AddPatientDetails(model.ClientKey);
                }

                if (Objects.IfNotNull(msgModel, i => i.SecureMessage.EMRMessageId) == 0) //Changed to portalmessageId
                {
                    using (var context = new MedflowContext(model.ClientKey))
                    {
                        if (msgModel != null)
                        {
                            var insertInboxQuery = string.Format("INSERT INTO Medflow.PatientPortalInbox(Message, ProviderId, MessageStatusId, Subject,  PatientId, CreatedDate, MessageTypeId, ExternalId, FromPatient, IsRead)" + "Values ( '{0}', {1}, 3, '{2}', {3}, '{4}', 2, {5}, 1, 0)", msgModel.SecureMessage.Message.Replace("'", "''"), msgModel.SecureMessage.ProviderId, msgModel.SecureMessage.Subject, msgModel.Patient.PatientAccount, msgModel.SecureMessage.RequestedDate, msgModel.SecureMessage.PortalMessageId);

                            business.LoggerExceptions("SaveSecureMessageFromPortal-0", "Inserting  into Medflow.PatientPortalInbox Table completed");
                            await Task.Run(() => context.Database.ExecuteSqlCommand(insertInboxQuery));
                            business.LoggerExceptions("SaveSecureMessageFromPortal-0", "Inserting  into Medflow.PatientPortalInbox Table completed");
                            var returnvalue = "SELECT CAST(Ident_Current(\'Medflow.PatientPortalInbox\') as int)   AS PortalInboxId";

                            msgModel.SecureMessage.PortalInboxId = context.Database.SqlQuery<int>(returnvalue).FirstOrDefault();
                            business.LoggerExceptions("SaveSecureMessageFromPortal", "Inserting  into Medflow.PatientPortalInbox Table completed");
                        }
                    }
                }
                else
                {
                    using (var context = new MedflowContext(model.ClientKey))
                    {
                        if (msgModel != null)
                        {

                            var _updateQuery = "Update Medflow.PatientPortalInbox Set IsRead = '0', LastUpdatedDate = GETDATE(), isClosed = NULL, ExternalId = '" + msgModel.SecureMessage.PortalMessageId + "'  Where Id = '" + msgModel.SecureMessage.EMRMessageId + "'";
                            await Task.Run(() => context.Database.ExecuteSqlCommand(_updateQuery));
                      
                            var insertInboxQuery = string.Format(
                                       "INSERT INTO medflow.PatientPortalInboxHistory(PatientPortalInboxId, EHRPortalInboxId, Description, SendUser, FromPatient) " +
                                       "Values ({0},{1},'{2}','{3}',1)",
                                       msgModel.SecureMessage.EMRMessageId,
                                       msgModel.SecureMessage.PortalMessageId, 
                                       msgModel.SecureMessage.Message.Replace("'", "''"),
                                       msgModel.SecureMessage.Sender
                                       );
                            await Task.Run(() => context.Database.ExecuteSqlCommand(insertInboxQuery));
                        }
                    }
                }

                try
                {
                    EmailNotification objENotification = new EmailNotification(msgModel.SecureMessage.ProviderId, model.ClientKey);
                    if (objENotification.EmailAddress != "")
                    {
                        int Status = EmailUtility.SecureMessageEmailNotifction(objENotification);
                        business.LoggerExceptions("SecureMessageEmailNotifction-Status", Status.ToString());
                    }
                }
                catch (Exception ee)
                {
                    business.LoggerExceptions("SecureMessageEmailNotifction", "Method Called" + ee.Message.ToString());
                }
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SaveSecureMessageFromPortal", "There's some issue while inserting records into PatientPortalInbox and PatientPortalInboxHistory Tables the error is -" + ex.Message);
                throw;
            }
        }

        public async Task<string> SavePatientDemographicsInfo(PatientDemographicsInformation model)
        {
            var business = new MedflowPortalBusiness();
            string result = "success";
            try
            {
                using (var context = new MedflowContext(model.ClientKey))
                {
                    if (model != null)
                    {
                        business.LoggerExceptions("SavePatientDemographicsInfo", "Inserting into the Medflow.PatientDemographicsInformation");
                        var query = String.Format("INSERT INTO Medflow.PatientDemographicsInformation (PatientAccount,LastName,FirstName,MiddleName,DOB,Gender) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')", model.PatientAccount, model.LastName.Replace("'", "''"), model.FirstName.Replace("'", "''"), model.MiddleName.Replace("'", "''"), model.Dob.ToString("MM/dd/yyyy"), model.Gender.Replace("'", "''"));

                        await Task.Run(() => context.Database.ExecuteSqlCommand(query));
                        business.LoggerExceptions("SavePatientDemographicsInfo", "Insert completed in Medflow.PatientDemographicsInformation ");
                    }
                }
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SavePatientDemographicsInfo", "Insertion failed due to the reason -" + ex.Message);
                result = "Failure";
            }
            return result;
        }

        public void LoggerExceptions(string methodName, string message)
        {
            try
            {

                string path = HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["ErrorFile"].ToString());

                using (StreamWriter writer = new StreamWriter(path, true))
                {

                    writer.WriteLine("Method Name :" + methodName + Environment.NewLine + "Message :" +
                       message + Environment.NewLine + "Date :" + DateTime.Now.ToString());

                    //writer.WriteLine("Message :" + "<br/>" + Environment.NewLine + "StackTrace :" +
                    //   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());

                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async static Task<int> SaveVDTInformation(PortalQueueXMLModel model)
        {
            //This is the XML that we are going to recieve from Medflow For Patient information
            //<Xml><Type>Patient View Health Audit</Type><Patient><FirstName></FirstName><LastName></LastName><MiddleName></MiddleName><PatientAccount></PatientAccount><DOB>MM/DD/YYYY</DOB> <Gender>MALE/FEMALE/UNKNOWN</Gender></Patient></Xml>

            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;
            try
            {
                //Parse the XML.So that it can be Saved in the VDT tables.
                string xmlRawMessage = model.MessageData;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlRawMessage);

                var xmlPatientAccountNode = xdoc.SelectSingleNode("//PatientAccount");
                var patientAccount = xmlPatientAccountNode != null ? (string.IsNullOrEmpty(xmlPatientAccountNode.InnerText) ? "0" : xmlPatientAccountNode.InnerText) : "0";

                var xmlProviderIdNode = xdoc.SelectSingleNode("//ProviderId");
                var providerId = xmlProviderIdNode != null ? (string.IsNullOrEmpty(xmlProviderIdNode.InnerText) ? "0" : xmlProviderIdNode.InnerText) : "0";

                if (Convert.ToInt32(patientAccount) > 0)
                {
                    //Save View Information.For MUCal Count
                    if (model.ActionTypeLookupTypeId == 5)
                    {
                        using (var db = new MedflowContext(model.ClientKey))
                        {
                            var query = " INSERT INTO MedFLow.VDTInformation(PatientId,EventType,EventDateTime,ProviderId) VALUES(" + patientAccount + ",'V',GETDATE()," + providerId + ") ";
                            rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                        }
                    }

                    //Save Download Information.For MUCal Count
                    if (model.ActionTypeLookupTypeId == 6)
                    {
                        using (var db = new MedflowContext(model.ClientKey))
                        {
                            var query = " INSERT INTO MedFLow.VDTInformation(PatientId,EventType,EventDateTime,ProviderId) VALUES(" + patientAccount + ",'D',GETDATE()," + providerId + ") ";

                            rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SaveVDTInformation", "Error while Saving VDT Information into Table -" + ex.Message);
            }
            return rowsaffected;
        }

        public async static Task<int> SavePatientAmendmentRequest(PortalQueueXMLModel model)
        {
            var business = new MedflowPortalBusiness();

            int rowsaffected = 0;
            try
            {
                //Parse the XML.So that it can be Saved in the VDT tables.
                string xmlRawMessage = model.MessageData;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlRawMessage);

                var xmlPatientAccountNode = xdoc.SelectSingleNode("//PatientAccount");
                var patientAccount = xmlPatientAccountNode != null ? (string.IsNullOrEmpty(xmlPatientAccountNode.InnerText) ? "0" : xmlPatientAccountNode.InnerText) : "0";

                var xmlExternalIdNode = xdoc.SelectSingleNode("//ExternalId");
                var ExternalId = xmlExternalIdNode != null ? (string.IsNullOrEmpty(xmlExternalIdNode.InnerText) ? "0" : xmlExternalIdNode.InnerText) : "0";

                var xmlMessageIdNode = xdoc.SelectSingleNode("//Message");
                var Message = xmlMessageIdNode != null ? (string.IsNullOrEmpty(xmlMessageIdNode.InnerText) ? "0" : xmlMessageIdNode.InnerText) : "0";

                var xmlRequestedDateNode = xdoc.SelectSingleNode("//RequestedDate");
                var RequestedDate = xmlRequestedDateNode != null ? (string.IsNullOrEmpty(xmlRequestedDateNode.InnerText) ? "0" : xmlRequestedDateNode.InnerText) : "0";

                var xmlSenderNode = xdoc.SelectSingleNode("//Sender");
                var Sender = xmlSenderNode != null ? (string.IsNullOrEmpty(xmlSenderNode.InnerText) ? "0" : xmlSenderNode.InnerText) : "0";

                var xmlProviderIdNode = xdoc.SelectSingleNode("//ProviderId");
                var ProviderId = xmlProviderIdNode != null ? (string.IsNullOrEmpty(xmlProviderIdNode.InnerText) ? "0" : xmlProviderIdNode.InnerText) : "0";

                if (Convert.ToInt32(patientAccount) > 0)
                {
                    if (model.ActionTypeLookupTypeId == 3)
                    {
                        using (var db = new MedflowContext(model.ClientKey))
                        {
                            var query = " INSERT INTO Medflow.PatientAmendmentInfo(PatientId, ExternalId, Message, RequestedDate, Sender, ProviderId, IsArcheive, IsRead) VALUES(" + patientAccount + ",'" + ExternalId + "', '" + Message.Replace("'", "''") + "','" + RequestedDate + "', '" + Sender + "', '" + ProviderId + "', '0', '0') ";
                            rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                        }
                    }
                }
                else
                {
                    business.LoggerExceptions("SavePatientAmendmentRequest", "Patient Account is null");
                }
            }

            catch (Exception ex)
            {
                business.LoggerExceptions("SavePatientAmendmentDetail", "Error while Saving Patient AmendmentDetail into Table -" + ex.Message);
            }
            return rowsaffected;
        }

        public async static Task<int> SavePatientAppointmentRequest(PortalQueueXMLModel model)
        {
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;
            try
            {
                //Parse the XML.So that it can be Saved in the VDT tables.
                string xmlRawMessage = model.MessageData;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlRawMessage);

                var xmlPatientAccountNode = xdoc.SelectSingleNode("//PatientAccount");
                var patientAccount = xmlPatientAccountNode != null ? (string.IsNullOrEmpty(xmlPatientAccountNode.InnerText) ? "0" : xmlPatientAccountNode.InnerText) : "0";

                var xmlExternalIdNode = xdoc.SelectSingleNode("//ExternalId");
                var ExternalId = xmlExternalIdNode != null ? (string.IsNullOrEmpty(xmlExternalIdNode.InnerText) ? "0" : xmlExternalIdNode.InnerText) : "0";

                var xmlMessageIdNode = xdoc.SelectSingleNode("//Description");
                var Message = xmlMessageIdNode != null ? (string.IsNullOrEmpty(xmlMessageIdNode.InnerText) ? "0" : xmlMessageIdNode.InnerText) : "0";

                var xmlRequestedDateNode = xdoc.SelectSingleNode("//RequestDate");
                var RequestedDate = xmlRequestedDateNode != null ? (string.IsNullOrEmpty(xmlRequestedDateNode.InnerText) ? "0" : xmlRequestedDateNode.InnerText) : DateTime.Now.ToString();

                var xmlSenderNode = xdoc.SelectSingleNode("//Sender");
                var Sender = xmlSenderNode != null ? (string.IsNullOrEmpty(xmlSenderNode.InnerText) ? "0" : xmlSenderNode.InnerText) : "0";

                var xmlProviderIdNode = xdoc.SelectSingleNode("//ProviderId");
                var ProviderId = xmlProviderIdNode != null ? (string.IsNullOrEmpty(xmlProviderIdNode.InnerText) ? "0" : xmlProviderIdNode.InnerText) : "0";

                var Subject = "";

                if (Convert.ToInt32(patientAccount) > 0)
                {
                    if (model.ActionTypeLookupTypeId == 28)
                    {
                        Subject = "New Appointment Request";
                    }
                    if (model.ActionTypeLookupTypeId == 17)
                    {
                        Subject = "Reschedule Appointment Request";
                    }
                    if (model.ActionTypeLookupTypeId == 18)
                    {
                        Subject = "Cancel Appointment Request";
                    }

                    using (var db = new MedflowContext(model.ClientKey))
                    {
                        var insertInboxQuery = string.Format("INSERT INTO Medflow.PatientPortalInbox(Message, ProviderId, MessageStatusId, Subject,  PatientId, CreatedDate, MessageTypeId, ExternalId, FromPatient, IsRead)" + "Values ( '{0}', {1}, 3, '{2}', {3}, '{4}', 2, {5}, 1, 0)", Message.Replace("'", "''"), ProviderId, Subject, patientAccount, RequestedDate, ExternalId);

                        business.LoggerExceptions("SavePatientAppointmentRequest", "Inserting  into Medflow.PatientPortalInbox Table completed");
                        await Task.Run(() => db.Database.ExecuteSqlCommand(insertInboxQuery));
                        business.LoggerExceptions("SavePatientAppointmentRequest", "Inserting  into Medflow.PatientPortalInbox Table completed");
                    }
                }
                else
                {
                    business.LoggerExceptions("SavePatientAppointmentRequest", "Patient Account is null");
                }
            }

            catch (Exception ex)
            {
                business.LoggerExceptions("SavePatientAppointmentRequest", "Error while Saving Patient AmendmentDetail into Table -" + ex.Message);
            }
            return rowsaffected;
        }

        public async static Task<int> SavePatientDemographicsRequest(PortalQueueXMLModel model)
        {
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;
            try
            {
                //Parse the XML.So that it can be Saved in the VDT tables.
                string xmlRawMessage = model.MessageData;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlRawMessage);

                var xmlPatientAccountNode = xdoc.SelectSingleNode("//PatientAccount");
                var patientAccount = xmlPatientAccountNode != null ? (string.IsNullOrEmpty(xmlPatientAccountNode.InnerText) ? "0" : xmlPatientAccountNode.InnerText) : "0";

                var xmlExternalIdNode = xdoc.SelectSingleNode("//PortalMessageId");
                var ExternalId = xmlExternalIdNode != null ? (string.IsNullOrEmpty(xmlExternalIdNode.InnerText) ? "0" : xmlExternalIdNode.InnerText) : "0";

                var xmlMessageIdNode = xdoc.SelectSingleNode("//Description");
                var Message = xmlMessageIdNode != null ? (string.IsNullOrEmpty(xmlMessageIdNode.InnerText) ? "0" : xmlMessageIdNode.InnerText) : "0";

                var xmlRequestedDateNode = xdoc.SelectSingleNode("//RequestedDate");
                var RequestedDate = xmlRequestedDateNode != null ? (string.IsNullOrEmpty(xmlRequestedDateNode.InnerText) ? "0" : xmlRequestedDateNode.InnerText) : DateTime.Now.ToString();

                var xmlSenderNode = xdoc.SelectSingleNode("//Sender");
                var Sender = xmlSenderNode != null ? (string.IsNullOrEmpty(xmlSenderNode.InnerText) ? "0" : xmlSenderNode.InnerText) : "0";

                var xmlProviderIdNode = xdoc.SelectSingleNode("//ProviderId");
                var ProviderId = xmlProviderIdNode != null ? (string.IsNullOrEmpty(xmlProviderIdNode.InnerText) ? "0" : xmlProviderIdNode.InnerText) : "0";

                if (Convert.ToInt32(patientAccount) > 0)
                {
                    if (model.ActionTypeLookupTypeId == 24)
                    {
                        using (var db = new MedflowContext(model.ClientKey))
                        {
                            var insertQuery = " INSERT INTO Medflow.PatientDemographicsRequest(PatientAccount, ExternalId, Description, RequestedDate, Sender, ProviderId, IsArchieve, IsRead) VALUES(" + patientAccount + ",'" + ExternalId + "', '" + Message.Replace("'", "''") + "','" + RequestedDate + "', '" + Sender + "', '" + ProviderId + "', '0', '0') ";

                            business.LoggerExceptions("SavePatientDemographicsRequest", "Inserting  into Medflow.PatientPortalInbox Table completed");
                            await Task.Run(() => db.Database.ExecuteSqlCommand(insertQuery));
                            business.LoggerExceptions("SavePatientDemographicsRequest", "Inserting  into Medflow.PatientPortalInbox Table completed");
                        }
                    }
                }
                else
                {
                    business.LoggerExceptions("SavePatientAppointmentRequest", "Patient Account is null");
                }
            }

            catch (Exception ex)
            {
                business.LoggerExceptions("SavePatientAppointmentRequest", "Error while Saving Patient AmendmentDetail into Table -" + ex.Message);
            }
            return rowsaffected;
        }

        public async static Task<int> SavePatientOcularMedicationRequest(PortalQueueXMLModel model)
        {
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;

            try
            {
                //Parse the XML.So that it can be Saved in the VDT tables.
                string xmlRawMessage = model.MessageData;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlRawMessage);

                var xmlPatientAccountNode = xdoc.SelectSingleNode("//PatientAccount");
                var patientAccount = xmlPatientAccountNode != null ? (string.IsNullOrEmpty(xmlPatientAccountNode.InnerText) ? "0" : xmlPatientAccountNode.InnerText) : "0";

                var xmlNameNode = xdoc.SelectSingleNode("//Name");
                var Name = xmlNameNode != null ? (string.IsNullOrEmpty(xmlNameNode.InnerText) ? "0" : xmlNameNode.InnerText) : "0";

                var xmlInstructionsNode = xdoc.SelectSingleNode("//Instructions");
                var Instructions = xmlInstructionsNode != null ? (string.IsNullOrEmpty(xmlInstructionsNode.InnerText) ? "0" : xmlInstructionsNode.InnerText) : "0";

                var xmlExternalIdNode = xdoc.SelectSingleNode("//ExternalId");
                var ExternalId = xmlExternalIdNode != null ? (string.IsNullOrEmpty(xmlExternalIdNode.InnerText) ? "0" : xmlExternalIdNode.InnerText) : "0";

                var xmlMessageIdNode = xdoc.SelectSingleNode("//Description");
                var Message = xmlMessageIdNode != null ? (string.IsNullOrEmpty(xmlMessageIdNode.InnerText) ? "0" : xmlMessageIdNode.InnerText) : "0";

                var xmlRequestedDateNode = xdoc.SelectSingleNode("//RequestedDate");
                var RequestedDate = xmlRequestedDateNode != null ? (string.IsNullOrEmpty(xmlRequestedDateNode.InnerText) ? "0" : xmlRequestedDateNode.InnerText) : DateTime.Now.ToString();

                var xmlSenderNode = xdoc.SelectSingleNode("//Sender");
                var Sender = xmlSenderNode != null ? (string.IsNullOrEmpty(xmlSenderNode.InnerText) ? "0" : xmlSenderNode.InnerText) : "0";

                var xmlProviderIdNode = xdoc.SelectSingleNode("//ProviderId");
                var ProviderId = xmlProviderIdNode != null ? (string.IsNullOrEmpty(xmlProviderIdNode.InnerText) ? "0" : xmlProviderIdNode.InnerText) : "0";

                if (Convert.ToInt32(patientAccount) > 0)
                {
                    if (model.ActionTypeLookupTypeId == 21)
                    {
                        using (var db = new MedflowContext(model.ClientKey))
                        {
                            var query = "INSERT INTO Medflow.PatientMedicationRequest(PatientAccount, ExternalId, Description, CreatedDate, Sender, ProviderId, IsRead, InstructionName, MedicineName) VALUES(" + patientAccount + ",'" + ExternalId + "', '" + Message.Replace("'", "''") + "','" + RequestedDate + "', '" + Sender + "', '" + ProviderId + "', '0', '" + Instructions.Replace("'", "''") + "', '" + Name.Replace("'", "''") + "') ";
                            rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                        }
                    }
                }
                else
                {
                    business.LoggerExceptions("SavePatientAppointmentRequest", "Patient Account is null");
                }
            }

            catch (Exception ex)
            {
                business.LoggerExceptions("SavePatientAppointmentRequest", "Error while Saving Patient AmendmentDetail into Table -" + ex.Message);
            }
            return rowsaffected;
        }

        public string GenerateXmlforAmendmentResponse(AmendmentResponseModel model)
        {
            string xml = string.Empty;
            string xsi = "http://www.w3.org/2001/XMLSchema-instance";
            string xsd = "http://www.w3.org/2001/XMLSchema";

            XmlDocument xmlDoc = new XmlDocument();
            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

            XmlElement documentElement = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            XmlSchema schema = new XmlSchema();
            schema.Namespaces.Add("xsi", xsi);
            schema.Namespaces.Add("xsd", xsd);
            xmlDoc.Schemas.Add(schema);

            XmlElement schemaNode = xmlDoc.CreateElement("Xml");
            schemaNode.SetAttribute("xmlns:xsi", xsi);
            schemaNode.SetAttribute("xmlns:xsd", xsd);
            xmlDoc.AppendChild(schemaNode);

            XmlNode rootNode = xmlDoc.CreateElement("Type");
            rootNode.InnerText = "Patient Amendment Response";
            schemaNode.AppendChild(rootNode);

            /*** Patient Part Starts here ***/

            XmlNode patient = xmlDoc.CreateElement("Patient");
            schemaNode.AppendChild(patient);

            XmlNode firstName = xmlDoc.CreateElement("FirstName");
            firstName.InnerText = Convert.ToString(model.FirstName);
            patient.AppendChild(firstName);

            XmlNode lastName = xmlDoc.CreateElement("LastName");
            lastName.InnerText = Convert.ToString(model.LastName);
            patient.AppendChild(lastName);


            XmlNode middleName = xmlDoc.CreateElement("MiddleName");
            middleName.InnerText = Convert.ToString(model.MiddleName);
            patient.AppendChild(middleName);


            XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
            patientAccount.InnerText = Convert.ToString(model.PatientAccount);
            patient.AppendChild(patientAccount);


            XmlNode dob = xmlDoc.CreateElement("DOB");
            dob.InnerText = Convert.ToDateTime(model.DOB).ToString("MM/dd/yyyy");
            patient.AppendChild(dob);

            XmlNode gender = xmlDoc.CreateElement("Gender");
            gender.InnerText = Convert.ToString(model.Gender);
            patient.AppendChild(gender);

            XmlNode EmailAddress = xmlDoc.CreateElement("Gender");
            gender.InnerText = Convert.ToString(model.Gender);
            patient.AppendChild(gender);

            XmlNode Amendment = xmlDoc.CreateElement("Amendment");
            schemaNode.AppendChild(Amendment);

            XmlNode ExternalId = xmlDoc.CreateElement("ExternalId");
            ExternalId.InnerText = Convert.ToString(model.ExternalId);
            Amendment.AppendChild(ExternalId);

            XmlNode message = xmlDoc.CreateElement("Message");
            message.InnerText = Convert.ToString(model.Message);
            Amendment.AppendChild(message);

            XmlNode ResponseDate = xmlDoc.CreateElement("ResponseDate");
            ResponseDate.InnerText = Convert.ToString(DateTime.Now.ToString());
            Amendment.AppendChild(ResponseDate);

            XmlNode ProviderId = xmlDoc.CreateElement("ProviderId");
            ProviderId.InnerText = model.ProviderId;
            Amendment.AppendChild(ProviderId);

            XmlNode Response = xmlDoc.CreateElement("Response");
            Response.InnerText = model.Response;
            Amendment.AppendChild(Response);     

            xml = xmlDoc.InnerXml;

            return xml;
        }

        public string GenerateXmlforDeliverDoctorResponse(DoctorResponsemodel model)
        {
            string xml = string.Empty;

            string xsi = "http://www.w3.org/2001/XMLSchema-instance";
            string xsd = "http://www.w3.org/2001/XMLSchema";

            XmlDocument xmlDoc = new XmlDocument();
            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

            XmlElement documentElement = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            XmlSchema schema = new XmlSchema();
            schema.Namespaces.Add("xsi", xsi);
            schema.Namespaces.Add("xsd", xsd);
            xmlDoc.Schemas.Add(schema);


            XmlElement schemaNode = xmlDoc.CreateElement("Xml");
            schemaNode.SetAttribute("xmlns:xsi", xsi);
            schemaNode.SetAttribute("xmlns:xsd", xsd);
            xmlDoc.AppendChild(schemaNode);

            XmlNode rootNode = xmlDoc.CreateElement("Type");
            rootNode.InnerText = "DeliverDoctorResponse";
            schemaNode.AppendChild(rootNode);

            /*** Patient Part Starts here ***/

            XmlNode patient = xmlDoc.CreateElement("Patient");
            schemaNode.AppendChild(patient);

            XmlNode firstName = xmlDoc.CreateElement("FirstName");
            firstName.InnerText = Convert.ToString(model.FirstName);
            patient.AppendChild(firstName);

            XmlNode lastName = xmlDoc.CreateElement("LastName");
            lastName.InnerText = Convert.ToString(model.LastName);
            patient.AppendChild(lastName);


            XmlNode middleName = xmlDoc.CreateElement("MiddleName");
            middleName.InnerText = Convert.ToString(model.MiddleName);
            patient.AppendChild(middleName);


            XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
            patientAccount.InnerText = Convert.ToString(model.PatientAccount);
            patient.AppendChild(patientAccount);


            XmlNode dob = xmlDoc.CreateElement("DOB");
            dob.InnerText = Convert.ToDateTime(model.DOB).ToString("MM/dd/yyyy");
            patient.AppendChild(dob);

            XmlNode gender = xmlDoc.CreateElement("Gender");
            gender.InnerText = Convert.ToString(model.Gender);
            patient.AppendChild(gender);

            /***Patient part ends here*****/



            XmlNode securemessage = xmlDoc.CreateElement("SecureMessage");
            schemaNode.AppendChild(securemessage);

            XmlNode Sender = xmlDoc.CreateElement("Sender");
            Sender.InnerText = "Patient";//Convert.ToString("");
            securemessage.AppendChild(Sender);

            XmlNode message = xmlDoc.CreateElement("Message");
            message.InnerText = Convert.ToString(model.Message);
            securemessage.AppendChild(message);

            XmlNode PortalMessageId = xmlDoc.CreateElement("PortalMessageId");
            PortalMessageId.InnerText = Convert.ToString(model.PortalMessageId);
            securemessage.AppendChild(PortalMessageId);

            XmlNode Subject = xmlDoc.CreateElement("Subject");
            Subject.InnerText = Convert.ToString(model.Subject);
            securemessage.AppendChild(Subject);

            XmlNode EMRMessageId = xmlDoc.CreateElement("EMRMessageId");
            EMRMessageId.InnerText = Convert.ToString(model.EMRMessageId);
            securemessage.AppendChild(EMRMessageId);


            XmlNode ProviderId = xmlDoc.CreateElement("ProviderId");
            ProviderId.InnerText = Convert.ToString(model.ProviderId);
            securemessage.AppendChild(ProviderId);


            xml = xmlDoc.InnerXml;

            return xml;
        }

        public string GenerateXmlforAppointmentResponse(AppointmentResponseModel model)
        {
            string xml = string.Empty;

            string xsi = "http://www.w3.org/2001/XMLSchema-instance";
            string xsd = "http://www.w3.org/2001/XMLSchema";

            XmlDocument xmlDoc = new XmlDocument();
            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

            XmlElement documentElement = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            XmlSchema schema = new XmlSchema();
            schema.Namespaces.Add("xsi", xsi);
            schema.Namespaces.Add("xsd", xsd);
            xmlDoc.Schemas.Add(schema);

            XmlElement schemaNode = xmlDoc.CreateElement("Xml");
            schemaNode.SetAttribute("xmlns:xsi", xsi);
            schemaNode.SetAttribute("xmlns:xsd", xsd);
            xmlDoc.AppendChild(schemaNode);

            XmlNode rootNode = xmlDoc.CreateElement("Type");
            rootNode.InnerText = "Appointment Response";
            schemaNode.AppendChild(rootNode);

            XmlNode patient = xmlDoc.CreateElement("Patient");
            schemaNode.AppendChild(patient);

            XmlNode firstName = xmlDoc.CreateElement("FirstName");
            firstName.InnerText = Convert.ToString(model.PatientDetail.FirstName);
            patient.AppendChild(firstName);

            XmlNode lastName = xmlDoc.CreateElement("LastName");
            lastName.InnerText = Convert.ToString(model.PatientDetail.LastName);
            patient.AppendChild(lastName);

            XmlNode middleName = xmlDoc.CreateElement("MiddleName");
            middleName.InnerText = Convert.ToString(model.PatientDetail.MiddleName);
            patient.AppendChild(middleName);

            XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
            patientAccount.InnerText = Convert.ToString(model.PatientDetail.PatientAccount);
            patient.AppendChild(patientAccount);

            XmlNode dob = xmlDoc.CreateElement("DOB");
            dob.InnerText = Convert.ToDateTime(model.PatientDetail.DOB).ToString("MM/dd/yyyy");
            patient.AppendChild(dob);

            XmlNode gender = xmlDoc.CreateElement("Gender");
            gender.InnerText = Convert.ToString(model.PatientDetail.Gender);
            patient.AppendChild(gender);

            XmlNode SecureMessage = xmlDoc.CreateElement("SecureMessage");
            schemaNode.AppendChild(SecureMessage);


            XmlNode RequestedDate = xmlDoc.CreateElement("RequestedDate");
            RequestedDate.InnerText = model.RequestedDate;
            SecureMessage.AppendChild(RequestedDate);

            XmlNode Sender = xmlDoc.CreateElement("Sender");
            Sender.InnerText = model.Sender;
            SecureMessage.AppendChild(Sender);

            XmlNode Message = xmlDoc.CreateElement("Message");
            Message.InnerText = model.Message;
            SecureMessage.AppendChild(Message);

            XmlNode PortalMessageId = xmlDoc.CreateElement("PortalMessageId");
            PortalMessageId.InnerText = model.PortalMessageId;
            SecureMessage.AppendChild(PortalMessageId);

            XmlNode Subject = xmlDoc.CreateElement("Subject");
            Subject.InnerText = Convert.ToString(model.Subject);
            SecureMessage.AppendChild(Subject);

            XmlNode EMRMessageId = xmlDoc.CreateElement("EMRMessageId");
            EMRMessageId.InnerText = model.EMRMessageId;
            SecureMessage.AppendChild(EMRMessageId);

            XmlNode ProviderId = xmlDoc.CreateElement("ProviderId");
            ProviderId.InnerText = model.ProviderId;
            SecureMessage.AppendChild(ProviderId);

            xml = xmlDoc.InnerXml;
            return xml;
        }

        public string GenerateXmlforOcularMedicationResponse(OcularMedicationResponseModel model)
        {
            string xml = string.Empty;

            string xsi = "http://www.w3.org/2001/XMLSchema-instance";
            string xsd = "http://www.w3.org/2001/XMLSchema";

            XmlDocument xmlDoc = new XmlDocument();
            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

            XmlElement documentElement = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            XmlSchema schema = new XmlSchema();
            schema.Namespaces.Add("xsi", xsi);
            schema.Namespaces.Add("xsd", xsd);
            xmlDoc.Schemas.Add(schema);

            XmlElement schemaNode = xmlDoc.CreateElement("Xml");
            schemaNode.SetAttribute("xmlns:xsi", xsi);
            schemaNode.SetAttribute("xmlns:xsd", xsd);
            xmlDoc.AppendChild(schemaNode);

            XmlNode rootNode = xmlDoc.CreateElement("Type");
            rootNode.InnerText = "Refill Response";
            schemaNode.AppendChild(rootNode);

            XmlNode patient = xmlDoc.CreateElement("Patient");
            schemaNode.AppendChild(patient);

            XmlNode firstName = xmlDoc.CreateElement("FirstName");
            firstName.InnerText = Convert.ToString(model.PatientDetail.FirstName);
            patient.AppendChild(firstName);

            XmlNode lastName = xmlDoc.CreateElement("LastName");
            lastName.InnerText = Convert.ToString(model.PatientDetail.LastName);
            patient.AppendChild(lastName);

            XmlNode middleName = xmlDoc.CreateElement("MiddleName");
            middleName.InnerText = Convert.ToString(model.PatientDetail.MiddleName);
            patient.AppendChild(middleName);

            XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
            patientAccount.InnerText = Convert.ToString(model.PatientDetail.PatientAccount);
            patient.AppendChild(patientAccount);

            XmlNode dob = xmlDoc.CreateElement("DOB");
            dob.InnerText = Convert.ToDateTime(model.PatientDetail.DOB).ToString("MM/dd/yyyy");
            patient.AppendChild(dob);

            XmlNode gender = xmlDoc.CreateElement("Gender");
            gender.InnerText = Convert.ToString(model.PatientDetail.Gender);
            patient.AppendChild(gender);

            XmlNode RefillResponse = xmlDoc.CreateElement("RefillResponse");
            schemaNode.AppendChild(RefillResponse);

            XmlNode ExternalId = xmlDoc.CreateElement("ExternalId");
            ExternalId.InnerText = model.ExternalId;
            RefillResponse.AppendChild(ExternalId);

            XmlNode Description = xmlDoc.CreateElement("Description");
            Description.InnerText = Convert.ToString(model.ReplyMessage);
            RefillResponse.AppendChild(Description);

            XmlNode ResponseDate = xmlDoc.CreateElement("RefillDate");
            ResponseDate.InnerText = Convert.ToString(model.ResponseDate);
            RefillResponse.AppendChild(ResponseDate);

            XmlNode Provider = xmlDoc.CreateElement("ProviderId");
            Provider.InnerText = Convert.ToString(model.ProviderId);
            RefillResponse.AppendChild(Provider);

            xml = xmlDoc.InnerXml;
            return xml;
        }

        public string GenerateXmlforPatientDemographicsResponse(PatientDemographicsResponse model)
        {
            string xml = string.Empty;

            string xsi = "http://www.w3.org/2001/XMLSchema-instance";
            string xsd = "http://www.w3.org/2001/XMLSchema";

            XmlDocument xmlDoc = new XmlDocument();
            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

            XmlElement documentElement = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            XmlSchema schema = new XmlSchema();
            schema.Namespaces.Add("xsi", xsi);
            schema.Namespaces.Add("xsd", xsd);
            xmlDoc.Schemas.Add(schema);

            XmlElement schemaNode = xmlDoc.CreateElement("Xml");
            schemaNode.SetAttribute("xmlns:xsi", xsi);
            schemaNode.SetAttribute("xmlns:xsd", xsd);
            xmlDoc.AppendChild(schemaNode);

            XmlNode rootNode = xmlDoc.CreateElement("Type");
            rootNode.InnerText = "Appointment Response";
            schemaNode.AppendChild(rootNode);

            XmlNode patient = xmlDoc.CreateElement("Patient");
            schemaNode.AppendChild(patient);

            XmlNode firstName = xmlDoc.CreateElement("FirstName");
            firstName.InnerText = Convert.ToString(model.PatientDetail.FirstName);
            patient.AppendChild(firstName);

            XmlNode lastName = xmlDoc.CreateElement("LastName");
            lastName.InnerText = Convert.ToString(model.PatientDetail.LastName);
            patient.AppendChild(lastName);

            XmlNode middleName = xmlDoc.CreateElement("MiddleName");
            middleName.InnerText = Convert.ToString(model.PatientDetail.MiddleName);
            patient.AppendChild(middleName);

            XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
            patientAccount.InnerText = Convert.ToString(model.PatientDetail.PatientAccount);
            patient.AppendChild(patientAccount);

            XmlNode dob = xmlDoc.CreateElement("DOB");
            dob.InnerText = Convert.ToDateTime(model.PatientDetail.DOB).ToString("MM/dd/yyyy");
            patient.AppendChild(dob);

            XmlNode gender = xmlDoc.CreateElement("Gender");
            gender.InnerText = Convert.ToString(model.PatientDetail.Gender);
            patient.AppendChild(gender);

            XmlNode SecureMessage = xmlDoc.CreateElement("SecureMessage");
            schemaNode.AppendChild(SecureMessage);


            XmlNode RequestedDate = xmlDoc.CreateElement("RequestedDate");
            RequestedDate.InnerText = model.RequestedDate.ToString();
            SecureMessage.AppendChild(RequestedDate);

            XmlNode Sender = xmlDoc.CreateElement("Sender");
            Sender.InnerText = model.Sender;
            SecureMessage.AppendChild(Sender);

            XmlNode Message = xmlDoc.CreateElement("Message");
            Message.InnerText = model.ReplyMessage;
            SecureMessage.AppendChild(Message);

            XmlNode PortalMessageId = xmlDoc.CreateElement("PortalMessageId");
            PortalMessageId.InnerText = model.ExternalId;
            SecureMessage.AppendChild(PortalMessageId);

            XmlNode Subject = xmlDoc.CreateElement("Subject");
            Subject.InnerText = Convert.ToString(model.Subject);
            SecureMessage.AppendChild(Subject);

            XmlNode EMRMessageId = xmlDoc.CreateElement("EMRMessageId");
            EMRMessageId.InnerText = "0";
            SecureMessage.AppendChild(EMRMessageId);

            XmlNode ProviderId = xmlDoc.CreateElement("ProviderId");
            ProviderId.InnerText = model.ProviderId;
            SecureMessage.AppendChild(ProviderId);

            xml = xmlDoc.InnerXml;
            return xml;
        }

    }
}