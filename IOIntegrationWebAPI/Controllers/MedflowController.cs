﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using IOIntegrationWebAPI.DataAccessLayer;
using IOIntegrationWebAPI.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Soaf.Collections;
using Newtonsoft.Json;

namespace IOIntegrationWebAPI.Controllers
{
    public class MedflowController : ApiController
    {
        // Method to Recieve all Requests From Medflow Portal
        [HttpPost]
        public async Task<HttpResponseMessage> PostToEMRPortalQueue(PortalQueueXMLModel model) //PortalQueueXMLModel model
        {
            var business = new MedflowPortalBusiness();
            business.LoggerExceptions("PostToEMRPortalQueue", "Calling the main method");
            try
            {
                if (model.ExternalId == 0 || string.IsNullOrEmpty(model.MessageData))
                {
                    business.LoggerExceptions("PostToEMRPortalQueue", "ExternalId or Message Data is missing!!!");
                }
                business.LoggerExceptions("PostToEMRPortalQueue", model.ClientKey);
                if (model.ExternalId != 0 && model.MessageData != "")
                {
                    var res = await business.SavePortalQueueXml(model);
                    switch (model.ActionTypeLookupTypeId)
                    {
                        case 1:
                            //This is for Saving secure messages from the portal
                            await MedflowPortalBusiness.SaveSecureMessageFromPortal(model);
                            break;
                        case 5:
                            //This is for View Save in it in VDT tables
                            await MedflowPortalBusiness.SaveVDTInformation(model);
                            break;
                        case 3:
                            //This is for Save Details in Amendment tables
                            await MedflowPortalBusiness.SavePatientAmendmentRequest(model);
                            break;
                        case 6:
                            //This is for Download Save in it in VDT tables
                            await MedflowPortalBusiness.SaveVDTInformation(model);
                            break;
                        case 28:
                            //This is for Save Details in Appointments tables
                            await MedflowPortalBusiness.SavePatientAppointmentRequest(model);
                            break;
                        case 17:
                            //This is for Save Details in Appointments tables
                            await MedflowPortalBusiness.SavePatientAppointmentRequest(model);
                            break;
                        case 18:
                            //This is for Save Details in Appointments tables
                            await MedflowPortalBusiness.SavePatientAppointmentRequest(model);
                            break;
                        case 21:
                            //This is for Save Details in Medication Refill tables
                            await MedflowPortalBusiness.SavePatientOcularMedicationRequest(model);
                            break;
                        case 24:
                            //This is for Save Details in Patient Demographics 
                            await MedflowPortalBusiness.SavePatientDemographicsRequest(model);
                            break;
                    }
                    var response = Request.CreateResponse(HttpStatusCode.OK, res);
                    return response;
                }
                else
                {
                    var response = Request.CreateResponse(HttpStatusCode.OK, "Fail");
                    return response;
                }
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("PostToEMRPortalQueue", "Error Method - " + ex.Message);
                var response = Request.CreateResponse(HttpStatusCode.OK, ex.Message);
                return response;
            }
        }


        //Action Method for Validating User
        [HttpGet]
        public IEnumerable<Validate> ValidateProviders(string UserName, string Password, string Clientkey)
        {
            IList<Validate> validate = new List<Validate>();
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(Clientkey);
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var PasswordParameter = new SqlParameter("@UserName", UserName);
                var PracticeIdParameter = new SqlParameter("@Password", Password);
                var resultnew =
                    context.Database.SqlQuery<Validate>("exec Medflow_ValidateProvider @UserName,@Password", PasswordParameter, PracticeIdParameter)
                        .ToList<Validate>();

                foreach (var r in resultnew)
                {
                    validate.Add(new Validate() { UserExists = r.UserExists, id = EncryptDecrypt.Encrypt(r.id), PracticeId = Clientkey, isAdmin = r.isAdmin, PracticeName = r.PracticeName, DisplayName = r.DisplayName, isAccountActivated = r.isAccountActivated });
                }
                return validate;
            }
        }

        [HttpGet]
        public IEnumerable<PasswordStatus> ResetOneTimePassword(string ProviderId, string EmailId, string OldPassword, string NewPassword, string Clientkey)
        {
            IList<PasswordStatus> validate = new List<PasswordStatus>();
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(Clientkey);
            string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var param_providerid = new SqlParameter("@ProviderId", DecryptedProviderId);
                var param_oldpassword = new SqlParameter("@OldPassword", OldPassword);
                var param_newpassword = new SqlParameter("@NewPassword", NewPassword);
                var param_emailid = new SqlParameter("@EmailId", EmailId);
                var resultnew = context.Database.SqlQuery<PasswordStatus>("Exec Medflow_ResetOneTimePassword @ProviderId, @EmailId, @OldPassword, @NewPassword", param_providerid, param_emailid, param_oldpassword, param_newpassword).ToList<PasswordStatus>();

                foreach (var r in resultnew)
                {
                    validate.Add(new PasswordStatus { ResetStatus = r.ResetStatus });
                }
                return validate;
            }
        }

        //localhost:62845/api/Medflow/ForgotPassword?ClientKey=310030003000&EmailId=ajain@iopracticeware.com
        [HttpGet]
        public int ForgotPassword(string Clientkey, string EmailId)
        {
            int retStatus = 0;
            ProviderDetials objProvider = new ProviderDetials("2", EmailId, Clientkey);
            if (objProvider.ProviderId == null)
            {
                retStatus = 2;
            }
            else
            {
                retStatus = EmailUtility.ForgotPassword(objProvider);
            }
            return retStatus;
        }

        [HttpGet]
        public IEnumerable<PasswordStatus> ChangePassword(string Clientkey, string ProviderId, string OldPassword, string NewPassword)
        {
            IList<PasswordStatus> validate = new List<PasswordStatus>();
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(Clientkey);
            string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var param_providerid = new SqlParameter("@ProviderId", DecryptedProviderId);
                var param_oldpassword = new SqlParameter("@OldPassword", OldPassword);
                var param_newpassword = new SqlParameter("@NewPassword", NewPassword);
                var resultnew = context.Database.SqlQuery<PasswordStatus>("Exec Medflow_ChangePassword @ProviderId, @OldPassword, @NewPassword", param_providerid, param_oldpassword, param_newpassword).ToList<PasswordStatus>();

                foreach (var r in resultnew)
                {
                    validate.Add(new PasswordStatus { ResetStatus = r.ResetStatus });
                }
                return validate;
            }
        }

        [HttpGet]
        public IEnumerable<PasswordStatus> ResetPassword(string Clientkey, string ProviderId, string NewPassword)
        {
            IList<PasswordStatus> validate = new List<PasswordStatus>();
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(Clientkey);
            string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var param_providerid = new SqlParameter("@ProviderId", DecryptedProviderId);
                var param_newpassword = new SqlParameter("@NewPassword", NewPassword);
                var resultnew = context.Database.SqlQuery<PasswordStatus>("Exec Medflow_ResetPassword @ProviderId, @NewPassword", param_providerid, param_newpassword).ToList<PasswordStatus>();

                foreach (var r in resultnew)
                {
                    validate.Add(new PasswordStatus { ResetStatus = r.ResetStatus });
                }
                return validate;
            }
        }

        [HttpGet]
        public IEnumerable<PortalInboxHistory> FetchChatHistory(int portalinboxid, string clientkey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(clientkey);
            IList<PortalInboxHistory> portalinboxhistory = new List<PortalInboxHistory>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var param_InboxId = new SqlParameter("@InboxId", portalinboxid);
                var resultnew = context.Database.SqlQuery<PortalInboxHistory>("Exec Medflow_fetchChatHistory @InboxId ", param_InboxId)
                        .ToList<PortalInboxHistory>();

                foreach (var r in resultnew)
                {
                    portalinboxhistory.Add(new PortalInboxHistory() { Description = r.Description, FromPatient = r.FromPatient, isClosed = r.isClosed, MessageId = r.MessageId });
                }
                return portalinboxhistory;
            }

        }

        [HttpGet] // 123
        public int SendReplytoPatient(string PortalInboxId, string ReplyMessage, string ClientKey, string FirstName, string LastName,
                                        string MiddleName, int PatientAccount, string DOB, string Gender, int ExternalId, string ProviderId,
                                        string Subject, string isAdmin)
        {
            var business = new MedflowPortalBusiness();
            business.LoggerExceptions("SendReplytoPatient", "SendReplytoPatient Start");
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            business.LoggerExceptions("SendReplytoPatient-PortalInboxId", PortalInboxId.ToString());
            int rowsaffected = 0;
            try
            {
                if (isAdmin == "Y")
                {
                    ReplyMessage = ReplyMessage + "<br />(Replied by Admin on behalf of Doctor)";
                }
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SavePortalQueueXml", "Error while inserting Medflow.PortalQueueXMl Table -" + ex.Message);
            }

            int DecryptedProviderId = Convert.ToInt32(EncryptDecrypt.Decrypt(ProviderId.ToString()));
            var doctorresponsemodel = new DoctorResponsemodel
            {
                FirstName = FirstName,
                LastName = LastName,
                MiddleName = MiddleName,
                DOB = DOB,
                Gender = Gender,
                Message = ReplyMessage.Replace("<br />","   "),
                PortalMessageId = ExternalId,
                EMRMessageId = PortalInboxId,
                Subject = Subject,
                ProviderId = DecryptedProviderId,
                PatientAccount = PatientAccount
            };

            var doctorresponsexml = business.GenerateXmlforDeliverDoctorResponse(doctorresponsemodel);

            using (var dbcontext = new MedflowContext(DecryptedPracticeId))
            {
                business.LoggerExceptions("SendReplytoPatient", "Inserting  into Medflow.PortalQueueXML Table");
                var insertportalqueuexmltable = string.Format("INSERT INTO Medflow.PortalQueueXML (MessageData, ActionTypeLookupId, ProcessedDate, PatientId, IsActive, ExternalId) " + "Values ('{0}',{1},'{2}',{3},{4},{5})", doctorresponsexml.Replace("'", "''"), 2, DateTime.Now.ToString(), PatientAccount, 1, ExternalId);
                dbcontext.Database.ExecuteSqlCommand(insertportalqueuexmltable);
                business.LoggerExceptions("SendReplytoPatient", "Insertion into Medflow.PortalQueueXML Table completed");
            }

            business.LoggerExceptions("SendReplytoPatient", "Creating Parameters for accessing the Medflow Url and sending the parameters to Medflow Patient Portal");

            //string EncryptedData = AESEncryptDecrypt.Encrypt(doctorresponsexml, DecryptedPracticeId); 

            var parametertosend = new
            {
                ClientKey = DecryptedPracticeId,
                ActionTypeLookupTypeId = 2,
                MessageData = doctorresponsexml,
                ExternalId = ExternalId,
                Patientid = PatientAccount,
                ProviderId = DecryptedProviderId,
                ProcessedDate = DateTime.Now.ToString()
            };


            try
            {
                business.LoggerExceptions("SendReplytoPatient", "Parameters has been Created and now accessing Medflow's shared Api");
                string result = string.Empty;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(parametertosend);
                string medflowurl = ConfigurationManager.AppSettings["medflowApiUrl"].ToString();

                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;

                business.LoggerExceptions("SendReplytoPatient", "Posting Created Parameters to the Medflow API");

                result = webclient.UploadString(medflowurl, "POST", json);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();

                business.LoggerExceptions("SendReplytoPatient", "Data has been uploaded on the Medflow Patient portal Database");
                if (result == "success")
                {
                    business.LoggerExceptions("SendReplytoPatient", "Success! Inserting into Medflow.PortalQueueXMLResponse Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        business.LoggerExceptions("SendReplytoPatient", "Inserting Records into Medflow.PatientPortalInboxHistory Table");
                        
                        string insertInboxHistoryQuery = "INSERT INTO Medflow.PatientPortalInboxHistory (PatientPortalInboxId, EHRPortalInboxId, Description, SendUser, FromPatient) Values ('" + PortalInboxId + "', '" + ExternalId + "', '" + ReplyMessage.Replace("'", "''") + "', '1', 0)";
                        db.Database.ExecuteSqlCommand(insertInboxHistoryQuery);
                        business.LoggerExceptions("SendReplytoPatient", "Insertion complete and EMRMessageId is returned");
                        business.LoggerExceptions("SendReplytoPatient", "Inserting into Medflow.PatientPortalInboxHistory Completed");

                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLResponse(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, OriginalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", parametertosend.MessageData.Replace("'", "''"), 2, DateTime.Now, parametertosend.Patientid, 1, parametertosend.ExternalId);
                        rowsaffected = db.Database.ExecuteSqlCommand(query);
                    }
                }
                else
                {
                    business.LoggerExceptions("SendReplytoPatient", "Failed! Inserting into Medflow.PortalQueueXMLException Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLException(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, ExternalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", parametertosend.MessageData, 2, DateTime.Now, parametertosend.Patientid, 1, parametertosend.ExternalId);
                        rowsaffected = db.Database.ExecuteSqlCommand(query);
                    }
                }
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SendReplytoPatient", "There's some issue with the function the Exception is -  " + ex.Message);
            }
            return rowsaffected;
        }

        [HttpGet]
        public async Task<int> SendAmendmentStatus(string ClientKey, string PatientFirstName, string PatientLastName, string PatientMiddleName, int PatientAccount, string PatientDOB, string PatientGender, string ExternalId, string ProviderId, string Sender, string AmendmentStatus, string RequestId)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;
            string Message = "";
            try
            {
                business.LoggerExceptions("SendAmendmentStatus", "Updating Status into Medflow.PatientAmendmentInfo Table");
                using (var db = new MedflowContext(DecryptedPracticeId))
                {
                    var UpdateAmendmentStatus = "Update Medflow.PatientAmendmentInfo Set ResponseStatus = '" + AmendmentStatus + "',ResponseDateTime = '" + DateTime.Now.ToString() + "' Where Id = '" + RequestId + "'";
                    await Task.Run(() => db.Database.ExecuteSqlCommand(UpdateAmendmentStatus));

                    Message = AmendmentDetail.FindMessage(RequestId, DecryptedPracticeId);

                }
                business.LoggerExceptions("SendAmendmentStatus", "Updating Status into Medflow.PatientAmendmentInfo Completed");
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SendAmendmentStatus", "Error while inserting Medflow.PortalQueueXMl Table -" + ex.Message);
            }

            string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId.ToString());
            var objAmendmentResponse = new AmendmentResponseModel
            {
                FirstName = PatientFirstName,
                LastName = PatientLastName,
                MiddleName = PatientMiddleName,
                DOB = PatientDOB,
                Gender = PatientGender,
                Message = AmendmentStatus == "0" ? Message + " - Your request has been rejected." : Message + " - Your request has been accepted.",
                ExternalId = ExternalId,
                ResponseDate = DateTime.Now.ToString(),
                Response = AmendmentStatus == "0" ? "Deny" : "Approve",
                PatientAccount = PatientAccount,
                ProviderId = DecryptedProviderId
            };

            var XML_AmendmentResponse = business.GenerateXmlforAmendmentResponse(objAmendmentResponse);

            using (var dbcontext = new MedflowContext(DecryptedPracticeId))
            {
                business.LoggerExceptions("SendAmendmentStatus", "Inserting  into Medflow.PortalQueueXML Table");

                var insertportalqueuexmltable = string.Format("INSERT INTO Medflow.PortalQueueXML(MessageData, ActionTypeLookupId, ProcessedDate, PatientId, IsActive, ExternalId) " + "Values ('{0}',{1},'{2}',{3},{4},{5})", XML_AmendmentResponse, 2, DateTime.Now.ToString(), PatientAccount, 1, ExternalId);

                await Task.Run(() => dbcontext.Database.ExecuteSqlCommand(insertportalqueuexmltable));
                business.LoggerExceptions("SendAmendmentStatus", "Insertion into Medflow.PortalQueueXML Table completed");
            }

            business.LoggerExceptions("SendAmendmentStatus", "Creating Parameters for accessing the Medflow Url and sending the parameters to Medflow Patient Portal");

            //string EncryptedData = AESEncryptDecrypt.Encrypt(XML_AmendmentResponse, DecryptedPracticeId);

            var Parameter_to_Send = new
            {
                ClientKey = DecryptedPracticeId,
                ActionTypeLookupTypeId = 3,
                MessageData = XML_AmendmentResponse,
                ExternalId = ExternalId,
                Patientid = PatientAccount,
                ProviderId = DecryptedProviderId,
                ProcessedDate = DateTime.Now.ToString()
            };

            try
            {
                business.LoggerExceptions("SendAmendmentStatus", "Parameters has been Created and now accessing Medflow's shared Api");
                string result = string.Empty;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(Parameter_to_Send);
                string medflowurl = ConfigurationManager.AppSettings["medflowApiUrl"].ToString();

                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;

                business.LoggerExceptions("SendAmendmentStatus", "Posting Created Parameters to the Medflow API");

                //Posting Created Parameters to the Medflow API

                result = webclient.UploadString(medflowurl, "POST", json);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();

                business.LoggerExceptions("SendAmendmentStatus", "Data has been uploaded on the Medflow Patient portal Database");

                if (result == "success")
                {
                    business.LoggerExceptions("SendAmendmentStatus", "Success! Inserting into Medflow.PortalQueueXMLResponse Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLResponse(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, OriginalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", Parameter_to_Send.MessageData, 2, DateTime.Now, Parameter_to_Send.Patientid, 1, Parameter_to_Send.ExternalId);
                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }
                else
                {
                    business.LoggerExceptions("SendAmendmentStatus", "Failed! Inserting into Medflow.PortalQueueXMLException Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLException(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, ExternalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", Parameter_to_Send.MessageData, 2, DateTime.Now, Parameter_to_Send.Patientid, 1, Parameter_to_Send.ExternalId);
                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SendAmendmentStatus", "There's some issue with the function the Exception is -  " + ex.Message);
            }
            return rowsaffected;
        }

        [HttpGet]
        public IEnumerable<MessageCount> FetchMessageCount(string providerid, string clientkey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(clientkey);
            IList<MessageCount> messagecount = new List<MessageCount>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var id = new SqlParameter("@ProviderId", EncryptDecrypt.Decrypt(providerid.ToString()));
                var resultnew = context.Database.SqlQuery<MessageCount>("Exec Medflow_FetchMessageCount @ProviderId", id).ToList<MessageCount>();
                foreach (var r in resultnew)
                {
                    messagecount.Add(new MessageCount() { Inbox = r.Inbox, Outbox = r.Outbox, Closed = r.Closed });
                }
                return messagecount;
            }
        }

        [HttpGet]
        public IEnumerable<PatientPortalInbox> FetchRecordsBasedonStatus(string typeid, string providerid, string clientkey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(clientkey);
            IList<PatientPortalInbox> ppi = new List<PatientPortalInbox>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var TypeId = new SqlParameter("@TypeId", typeid);
                var ProviderId = new SqlParameter("@ProviderId", EncryptDecrypt.Decrypt(providerid.ToString()));
                var securemessagelist = context.Database.SqlQuery<PatientPortalInbox>("Exec Medflow_FetchRecordsBasedonStatus @TypeId, @ProviderId", TypeId, ProviderId).ToList<PatientPortalInbox>();
                foreach (var smessage in securemessagelist)
                {
                    ppi.Add(new PatientPortalInbox() { ProviderId = smessage.ProviderId, Subject = smessage.Subject, PatientId = smessage.PatientId, CreatedDate = TimeZoneInfo.ConvertTimeToUtc(smessage.CreatedDate), ExternalId = smessage.ExternalId, Id = smessage.Id, FirstName = smessage.FirstName, LastName = smessage.LastName, DOB = smessage.DOB, Gender = smessage.Gender, MiddleName = smessage.MiddleName, IsRead = smessage.IsRead });
                }
                return ppi;
            }
        }

        [HttpGet]
        public IEnumerable<Providerinfo> fetchalltherestproviders(string providerid, string practiceid)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(practiceid);
            IList<Providerinfo> providerinfo = new List<Providerinfo>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var _providerid = new SqlParameter("@Providerid", EncryptDecrypt.Decrypt(providerid.ToString()));
                var _practiceid = new SqlParameter("@practiceid", DecryptedPracticeId);

                // var clientid = new SqlParameter("@clientid", clientid);

                var _providerlist =
                    context.Database.SqlQuery<Providerinfo>("exec Medflow_FetchProviders @Providerid,@practiceid", _providerid, _practiceid)
                        .ToList<Providerinfo>();

                foreach (var _providerinfo in _providerlist)
                {
                    string EncryptedProviderId = EncryptDecrypt.Encrypt(_providerinfo.id);
                    providerinfo.Add(new Providerinfo() { id = EncryptedProviderId, displayname = _providerinfo.displayname, firstname = _providerinfo.firstname, LastName = _providerinfo.LastName });

                }
                return providerinfo;
            }

        }

        [HttpPost]
        public IEnumerable<CloseMessage> closemessagethread(int? portalinboxid, string clientkey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(clientkey);
            IList<CloseMessage> returnvalue = new List<CloseMessage>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var _portalinboxid = new SqlParameter("@portalinboxid", portalinboxid);
                var _clientid = new SqlParameter("@clientid", DecryptedPracticeId);
                var _returnvalue =
                    context.Database.SqlQuery<CloseMessage>("Exec Medflow_CloseMessageThread @portalinboxid,@clientid", _portalinboxid, _clientid)
                        .ToList<CloseMessage>();

                foreach (var _value in _returnvalue)
                {
                    returnvalue.Add(new CloseMessage() { closed = _value.closed });
                }
                return returnvalue;
            }

        }

        [HttpPost]
        public IEnumerable<UserProfile> fetchuserprofile(int? providerid, string clientkey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(clientkey);
            IList<UserProfile> userprofile = new List<UserProfile>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var _providerid = new SqlParameter("@providerId", EncryptDecrypt.Decrypt(providerid.ToString()));
                var _clientkey = new SqlParameter("@clientKey", DecryptedPracticeId);
                var _userprofile =
                    context.Database.SqlQuery<UserProfile>("exec Medflow_FetchUserProfile @providerId,@clientKey", _providerid, _clientkey)
                        .ToList<UserProfile>();

                foreach (var _user in _userprofile)
                {
                    userprofile.Add(new UserProfile() { DisplayName = _user.DisplayName, FirstName = _user.FirstName, LastName = _user.LastName, PID = _user.PID, PracticeName = _user.PracticeName, isAdmin = _user.isAdmin });
                }
                return userprofile;
            }
        }

        [HttpPost]
        public IEnumerable<PatientModel> FetchPatients(string LastName, string clientkey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(clientkey);
            IList<PatientModel> patientlist = new List<PatientModel>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var _lastname = new SqlParameter("@Lastname", LastName);
                var clientid = new SqlParameter("@clientid", DecryptedPracticeId);

                var _patientlist =
                    context.Database.SqlQuery<PatientModel>("exec Medflow_GetPatientDetailByLastName @Lastname,@clientid", _lastname, clientid)
                        .ToList<PatientModel>();

                foreach (var _patient in _patientlist)
                {
                    patientlist.Add(new PatientModel() { PatientAccount = _patient.PatientAccount, FirstName = _patient.FirstName, LastName = _patient.LastName, MiddleName = _patient.MiddleName, DOB = _patient.DOB, Gender = _patient.Gender });

                }
                return patientlist;
            }
        }

        [HttpPost]
        public async Task<string> InsertPatientDemographicsInfo(PatientDemographicsInformation model)
        {
            string result = "";
            var business = new MedflowPortalBusiness();
            if (model != null)
            {
                Patient objPatient = new Patient(model.PatientAccount.ToString(), model.ClientKey);
                if (objPatient.LastName == null)
                {
                    result = await business.SavePatientDemographicsInfo(model);
                }
                else 
                {
                    result = "success";
                }
            }
            return result;
        }

        [HttpGet]
        public IEnumerable<AmendmentsStats> FetchAllAmemdmentStats(string ProviderId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            IList<AmendmentsStats> objStats = new List<AmendmentsStats>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
                var param_providerid = new SqlParameter("@ProviderId", DecryptedProviderId);
                var _returnvalue =
                    context.Database.SqlQuery<AmendmentsStats>("Exec Medflow_AmendmentsStats @ProviderId ", param_providerid)
                        .ToList<AmendmentsStats>();

                foreach (var _value in _returnvalue)
                {
                    objStats.Add(new AmendmentsStats() { NewRequest = _value.NewRequest, CloseRequest = _value.CloseRequest, PendingRequest = _value.PendingRequest });

                }
                return objStats;
            }
        }

        [HttpGet]
        public IEnumerable<AmendmentRequest> FetchAmemdmentRequest(string ProviderId, string RequestType, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            IList<AmendmentRequest> objRequestList = new List<AmendmentRequest>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var param_providerid = new SqlParameter("@ProviderId", EncryptDecrypt.Decrypt(ProviderId));
                var param_requestType = new SqlParameter("@Status", RequestType);
                var _returnvalue =
                    context.Database.SqlQuery<AmendmentRequest>("Exec Medflow_FetchAmendmentRequest @ProviderId, @Status", param_providerid, param_requestType)
                        .ToList<AmendmentRequest>();

                if (RequestType.ToLower() == "close")
                {
                    foreach (var _value in _returnvalue)
                    {
                        objRequestList.Add(new AmendmentRequest() { RequestId = _value.RequestId, PatientId = _value.PatientId, Sender = _value.Sender, ExternalId = _value.ExternalId, Message = _value.Message, ProviderId = ProviderId, RequestedDate = TimeZoneInfo.ConvertTimeToUtc(_value.RequestedDate), ResponseDateTime = TimeZoneInfo.ConvertTimeToUtc(_value.ResponseDateTime), ResponseStatus = _value.ResponseStatus, PatientDOB = _value.PatientDOB, PatientFirstName = _value.PatientFirstName, PatientLastName = _value.PatientLastName, PatientGender = _value.PatientGender, PatientMiddleName = _value.PatientMiddleName });
                    }
                }
                else 
                {
                    foreach (var _value in _returnvalue)
                    {
                        objRequestList.Add(new AmendmentRequest() { RequestId = _value.RequestId, PatientId = _value.PatientId, Sender = _value.Sender, ExternalId = _value.ExternalId, Message = _value.Message, ProviderId = ProviderId, RequestedDate = TimeZoneInfo.ConvertTimeToUtc(_value.RequestedDate), ResponseStatus = _value.ResponseStatus, PatientDOB = _value.PatientDOB, PatientFirstName = _value.PatientFirstName, PatientLastName = _value.PatientLastName, PatientGender = _value.PatientGender, PatientMiddleName = _value.PatientMiddleName });
                    }
                }
                return objRequestList;
            }
        }

        [HttpGet]
        public IEnumerable<PasswordStatus> UpdateAmendmentStatus(string ProviderId, string RequestId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            IList<PasswordStatus> retStatus = new List<PasswordStatus>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var param_providerId = new SqlParameter("@ProviderId", EncryptDecrypt.Decrypt(ProviderId));
                var param_requestId = new SqlParameter("@RequestId", RequestId);
                var _returnvalue = context.Database.SqlQuery<PasswordStatus>("Exec Medflow_ReadAmendmentRequest @ProviderId, @RequestId", param_providerId, param_requestId).ToList<PasswordStatus>();
                foreach (var _value in _returnvalue)
                {
                    retStatus.Add(new PasswordStatus() { ResetStatus = _value.ResetStatus });
                }
                return retStatus;
            }
        }

        [HttpGet]
        public List<PatientDetail> GetPatientList(string PatientPrefix, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            List<PatientDetail> objPatientList = new List<PatientDetail>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var param_patientprefix = new SqlParameter("@PatientPrefix", PatientPrefix);
                var _returnvalue = context.Database.SqlQuery<Patient>("Exec Medflow_GetPatientList @PatientPrefix", param_patientprefix).ToList<Patient>();
                foreach (var _value in _returnvalue)
                {
                    objPatientList.Add(new PatientDetail()
                    {
                        PatientId = _value.PatientAccount,
                        PatientDesc = _value.FirstName + " " + _value.MiddleName + " " +
                            _value.LastName + "_" + _value.Gender + "_" + _value.DOB
                    });
                }
                return objPatientList;
            }
        }

        [HttpGet]
        public int ReadStatus_Message(string InboxId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            int retStatus = 0;
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                try
                {
                    var Param_InboxId = new SqlParameter("@InboxId", Convert.ToInt16(InboxId));
                    context.Database.ExecuteSqlCommand("Update Medflow.PatientPortalInbox Set IsRead = '1' Where Id = @InboxId", Param_InboxId);
                    retStatus = 1;
                }
                catch (Exception ee)
                {
                    retStatus = 0;
                }
                return retStatus;
            }
        }

        [HttpGet]
        public int RemoveAlertDetails(string ProviderId, string ClientKey)
        {
            int retStatus = 1;
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                try
                {
                    string _deleteQuery = "Delete From Medflow.User_AlertDetails Where ProviderId = '" + ProviderId + "'";
                    retStatus = context.Database.ExecuteSqlCommand(_deleteQuery);
                }
                catch (Exception e)
                {
                    retStatus = 0;
                }
            }
            return retStatus;
        }

        [HttpGet]
        public int UpdateProviderAlert(string ProviderId, string EmailAddress, string ClientKey)
        {
            int retStatus = 1;
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                try
                {
                    string _InsertQuery = "Insert into Medflow.User_AlertDetails(ProviderId, EmailAddress, IsActive, LastUpdatedDate) Values ('" + ProviderId + "', '" + EmailAddress + "','0',GETDATE())";
                    retStatus = context.Database.ExecuteSqlCommand(_InsertQuery);
                }
                catch (Exception e)
                {
                    retStatus = 0;
                }
            }
            return retStatus;
        }

        [HttpGet]
        public IEnumerable<EmailNotificationEntity> FetchUserAlertsList(string ProviderId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            List<EmailNotificationEntity> objProviderList = new List<EmailNotificationEntity>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
                var Param_ProviderId = new SqlParameter("@ProviderId", DecryptedProviderId);
                var _returnvalue = context.Database.SqlQuery<EmailNotificationEntity>("Exec Medflow_UserAlertsList @ProviderId", Param_ProviderId).ToList<EmailNotificationEntity>();
                foreach (var _value in _returnvalue)
                {
                    objProviderList.Add(new EmailNotificationEntity()
                    {
                        ProviderId = _value.ProviderId,
                        DisplayName = _value.DisplayName,
                        EmailAddress = _value.EmailAddress,
                        Status = _value.Status,
                        Dated = _value.Dated
                    });
                }
                return objProviderList;
            }
        }

        [HttpGet]
        public async Task<int> ComposeNewMessage(string Message, string ClientKey, string PatientAccount, int ExternalId, string ProviderId, string Subject)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;
            string returnvalue;
            int PatientPortalInboxId = 0;
            Patient objPatientDetail = new Patient(PatientAccount, DecryptedPracticeId);

            int DecryptedProviderId = Convert.ToInt32(EncryptDecrypt.Decrypt(ProviderId.ToString()));
            try
            {
                using (var db = new MedflowContext(DecryptedPracticeId))
                {
                    var _InboxQuery = "INSERT INTO Medflow.PatientPortalInbox(Message, ProviderId, MessageStatusId, Subject,  PatientId, CreatedDate, MessageTypeId, ExternalId, FromPatient, IsRead, LastUpdatedDate) Values ('" + Message.Replace("'", "''") + "', '" + DecryptedProviderId + "', 4, '" + Subject + "', '" + PatientAccount + "', '" + DateTime.Now.ToString() + "', 2, '0', '0', '1', GETDATE())";
                    await Task.Run(() => db.Database.ExecuteSqlCommand(_InboxQuery));
                    business.LoggerExceptions("ComposeNewMessage 12", "Inserted  into Medflow.PatientPortalInbox Table ");
                    returnvalue = "SELECT CAST(Ident_Current(\'Medflow.PatientPortalInbox\') as int) AS Id";
                    PatientPortalInboxId = db.Database.SqlQuery<int>(returnvalue).FirstOrDefault();
                }

                business.LoggerExceptions("ComposeNewMessage", "Inserting into Medflow.PatientPortalInbox Completed");

            }
            catch (Exception ex)
            {
                business.LoggerExceptions("ComposeNewMessage Exception", "Error while inserting Medflow.PatientPortalInbox Table -" + ex.Message);
            }

            var doctorresponsemodel = new DoctorResponsemodel
            {
                FirstName = objPatientDetail.FirstName,
                LastName = objPatientDetail.LastName,
                MiddleName = objPatientDetail.MiddleName,
                DOB = objPatientDetail.DOB,
                Gender = objPatientDetail.Gender,
                Message = Message,
                PortalMessageId = ExternalId,
                EMRMessageId = PatientPortalInboxId.ToString(),
                Subject = Subject,
                ProviderId = DecryptedProviderId,
                PatientAccount = Convert.ToInt32(PatientAccount)
            };

            var doctorresponsexml = business.GenerateXmlforDeliverDoctorResponse(doctorresponsemodel);

            using (var dbcontext = new MedflowContext(DecryptedPracticeId))
            {
                business.LoggerExceptions("ComposeNewMessage", "Inserting  into Medflow.PortalQueueXML Table");

                var insertportalqueuexmltable = string.Format(
                    "INSERT INTO Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive,ExternalId) " +
                    "Values ('{0}',{1},'{2}',{3},{4},{5})",
                    doctorresponsexml.Replace("'","''"),
                    2,
                    DateTime.Now.ToString(),
                    PatientAccount,
                    1,
                    ExternalId
                    );

                await Task.Run(() => dbcontext.Database.ExecuteSqlCommand(insertportalqueuexmltable));
                business.LoggerExceptions("ComposeNewMessage", "Insertion into Medflow.PortalQueueXML Table completed");
            }

            business.LoggerExceptions("ComposeNewMessage", "Creating Parameters for accessing the Medflow Url and sending the parameters to Medflow Patient Portal");

            var parametertosend = new
            {
                ClientKey = DecryptedPracticeId,
                ActionTypeLookupTypeId = 2,
                MessageData = doctorresponsexml,
                ExternalId = ExternalId,
                Patientid = PatientAccount,
                ProviderId = DecryptedProviderId,
                ProcessedDate = DateTime.Now.ToString()
            };

            try
            {
                business.LoggerExceptions("ComposeNewMessage", "Parameters has been Created and now accessing Medflow's shared Api");
                string result = string.Empty;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(parametertosend);
                string medflowurl = ConfigurationManager.AppSettings["medflowApiUrl"].ToString();

                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;

                business.LoggerExceptions("ComposeNewMessage", "Posting Created Parameters to the Medflow API");

                result = webclient.UploadString(medflowurl, "POST", json);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();

                business.LoggerExceptions("ComposeNewMessage", "Data has been uploaded on the Medflow Patient portal Database");

                if (result == "success")
                {
                    business.LoggerExceptions("ComposeNewMessage", "Success! Inserting into Medflow.PortalQueueXMLResponse Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLResponse(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, OriginalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", parametertosend.MessageData, 2, DateTime.Now, parametertosend.Patientid, 1, parametertosend.ExternalId);
                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }
                else
                {
                    business.LoggerExceptions("ComposeNewMessage", "Failed! Inserting into Medflow.PortalQueueXMLException Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLException(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, ExternalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", parametertosend.MessageData, 2, DateTime.Now, parametertosend.Patientid, 1, parametertosend.ExternalId);

                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }
            }
            catch (Exception ex)
            {
                using (var db = new MedflowContext(DecryptedPracticeId))
                {
                    var deleteQuery = "Delete From Medflow.PatientPortalInbox Where Id = '" + PatientPortalInboxId + "'";
                    rowsaffected = db.Database.ExecuteSqlCommand(deleteQuery);
                }
                business.LoggerExceptions("ComposeNewMessage", "There's some issue with the function the Exception is -  " + ex.Message);
            }
            return rowsaffected;
        }
        

        [HttpGet]
        public IEnumerable<OcularMedicationDetail> FetchOcularMedicationRequest(string ProviderId, string TypeId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            List<OcularMedicationDetail> objMedicationList = new List<OcularMedicationDetail>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
                var Param_ProviderId = new SqlParameter("@ProviderId", DecryptedProviderId);
                var Param_TypeId = new SqlParameter("@Status", TypeId);
                var _returnvalue = context.Database.SqlQuery<OcularMedicationDetail>("Exec Medflow_FetchOcularMedicationRequest @ProviderId, @Status", Param_ProviderId, Param_TypeId).ToList<OcularMedicationDetail>();
                foreach (var _value in _returnvalue)
                {
                    objMedicationList.Add(new OcularMedicationDetail()
                    {
                        RequestId = _value.RequestId,
                        ProviderId = ProviderId,
                        Description = _value.Description,
                        DOB = _value.DOB,
                        ExternalId = _value.ExternalId,
                        FirstName = _value.FirstName,
                        IsRead = _value.IsRead,
                        LastName = _value.LastName,
                        PatientAccount = _value.PatientAccount,
                        RequestedDate = _value.RequestedDate,
                        ResponseDate = _value.ResponseDate,
                        RepliedMessage = _value.RepliedMessage,
                        InstructionName = _value.InstructionName,
                        MedicineName = _value.MedicineName
                    });
                }
                return objMedicationList;
            }
        }

        [HttpGet]
        public IEnumerable<OcularMedicationStats> FetchOcularMedicationStats(string ProviderId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            List<OcularMedicationStats> objAppointmentStats = new List<OcularMedicationStats>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
                var param_providerId = new SqlParameter("@ProviderId", DecryptedProviderId);
                var _returnvalue = context.Database.SqlQuery<OcularMedicationStats>("Exec Medflow_OcularMedicationStats @ProviderId", param_providerId).ToList<OcularMedicationStats>();
                foreach (var _value in _returnvalue)
                {
                    objAppointmentStats.Add(new OcularMedicationStats()
                    {
                        NewRequest = _value.NewRequest,
                        PendingRequest = _value.PendingRequest,
                        CloseRequest = _value.CloseRequest
                    });
                }
                return objAppointmentStats;
            }
        }

        [HttpGet]
        public int ReadRequest_OcularMedication(string RequestId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            int retStatus = 0;
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                try
                {
                    var Param_RequestId = new SqlParameter("@RequestId", RequestId);
                    context.Database.ExecuteSqlCommand("Update Medflow.PatientMedicationRequest Set IsRead = '1' Where RequestId = @RequestId", Param_RequestId);
                    retStatus = 1;
                }
                catch (Exception ee)
                {
                    retStatus = 0;
                }
                return retStatus;
            }
        }

        [HttpGet]
        public async Task<int> SendOcularMedicationResponse(string ClientKey, int PatientAccount, string ExternalId, string ReplyMessage, string RequestId, string ProviderId)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;

            string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId.ToString());
            ReplyMessage = ReplyMessage == "0" ? "Your request has been rejected" : "Your request has been accepted";
            var RefillResponse = new OcularMedicationResponseModel
            {
                PatientDetail = new Patient(PatientAccount.ToString(), DecryptedPracticeId),
                ReplyMessage = ReplyMessage,
                ExternalId = ExternalId,
                ProviderId = ProviderId,
                ResponseDate = DateTime.Now.ToString(),
            };

            var XML_Response = business.GenerateXmlforOcularMedicationResponse(RefillResponse);

            using (var dbcontext = new MedflowContext(DecryptedPracticeId))
            {
                business.LoggerExceptions("SendOcularMedicationResponse", "Inserting  into Medflow.PortalQueueXML Table");

                var insertportalqueuexmltable = string.Format("INSERT INTO Medflow.PortalQueueXML(MessageData, ActionTypeLookupId, ProcessedDate, PatientId, IsActive, ExternalId) " + "Values ('{0}',{1},'{2}',{3},{4},{5})", XML_Response, 22, DateTime.Now.ToString(), PatientAccount, 1, ExternalId);

                await Task.Run(() => dbcontext.Database.ExecuteSqlCommand(insertportalqueuexmltable));
                business.LoggerExceptions("SendOcularMedicationResponse", "Insertion into Medflow.PortalQueueXML Table completed");
            }

            //string EncryptedData = AESEncryptDecrypt.Encrypt(XML_Response, DecryptedPracticeId);

            var Parameter_to_Send = new
            {
                ClientKey = DecryptedPracticeId,
                ActionTypeLookupTypeId = 22,
                MessageData = XML_Response,
                ExternalId = ExternalId,
                Patientid = PatientAccount,
                ProviderId = DecryptedProviderId,
                ProcessedDate = DateTime.Now.ToString()
            };

            try
            {
                business.LoggerExceptions("SendOcularMedicationResponse", "Parameters has been Created and now accessing Medflow's shared Api");
                string result = string.Empty;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(Parameter_to_Send);
                string medflowurl = ConfigurationManager.AppSettings["medflowApiUrl"].ToString();

                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;

                business.LoggerExceptions("SendOcularMedicationResponse", "Posting Created Parameters to the Medflow API");

                //Posting Created Parameters to the Medflow API

                result = webclient.UploadString(medflowurl, "POST", json);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();

                business.LoggerExceptions("SendOcularMedicationResponse", "Data has been uploaded on the Medflow Patient portal Database");

                if (result == "success")
                {
                    business.LoggerExceptions("SendOcularMedicationResponse", "Success! Inserting into Medflow.PortalQueueXMLResponse Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLResponse(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, OriginalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", Parameter_to_Send.MessageData, 2, DateTime.Now, Parameter_to_Send.Patientid, 1, Parameter_to_Send.ExternalId);
                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }
                else
                {
                    business.LoggerExceptions("SendOcularMedicationResponse", "Failed! Inserting into Medflow.PortalQueueXMLException Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLException(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, ExternalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", Parameter_to_Send.MessageData, 2, DateTime.Now, Parameter_to_Send.Patientid, 1, Parameter_to_Send.ExternalId);
                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }

                business.LoggerExceptions("SendOcularMedicationResponse", "Updating Status into Medflow.PatientAmendmentInfo Table");
                using (var db = new MedflowContext(DecryptedPracticeId))
                {
                    var _updateQuery = "Update Medflow.PatientMedicationRequest Set RepliedMessage = '" + ReplyMessage.Replace("'", "''") + "', ResponseDate = '" + DateTime.Now.ToString() + "' Where RequestId = '" + RequestId + "'";
                    await Task.Run(() => db.Database.ExecuteSqlCommand(_updateQuery));
                }
                business.LoggerExceptions("SendOcularMedicationResponse", "Updating Status into Medflow.PatientAmendmentInfo Completed");
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SendOcularMedicationResponse", "Error while inserting Medflow.PortalQueueXMl Table -" + ex.Message);
            }

            return rowsaffected;
        }

        [HttpGet]
        public UserCredentials UpdateNewUser(string PracticeId, string ProviderId, string DisplayName, string FirstName, string LastName, string Pin, string PracticeName, string isAdmin)
        {
            UserCredentials UC = null;
            Users objTemp = new Users(ProviderId, PracticeId);
            if (objTemp.ProviderId == "")
            {
                objTemp.ProviderId = ProviderId;
                objTemp.DisplayName = DisplayName;
                objTemp.FirstName = FirstName;
                objTemp.LastName = LastName;
                objTemp.Pin = Pin;
                objTemp.PracticeName = PracticeName;
                objTemp.IsAdmin = isAdmin;
                objTemp.PracticeId = PracticeId;
                UC = objTemp.RegisterNewUser();
            }
            else 
            {
                if (objTemp.Crdentials.UserName == "")
                {
                    UC = objTemp.RegisterUserCredentials();
                }
                else 
                {
                    UC = objTemp.Crdentials;
                }
            }
            return UC; 
        }

        [HttpGet]
        public IEnumerable<PatientDemogrphicsDetail> FetchPatientDemographicsRequest(string TypeId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            List<PatientDemogrphicsDetail> objRequestList = new List<PatientDemogrphicsDetail>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var Param_TypeId = new SqlParameter("@TypeId", TypeId);
                var _returnvalue = context.Database.SqlQuery<PatientDemogrphicsDetail>("Exec Medflow_FetchPatientDemographicsRequest @TypeId", Param_TypeId).ToList<PatientDemogrphicsDetail>();
                foreach (var _value in _returnvalue)
                {
                    objRequestList.Add(new PatientDemogrphicsDetail()
                    {
                        RequestId = _value.RequestId,
                        Description = _value.Description,
                        DOB = _value.DOB,
                        ExternalId = _value.ExternalId,
                        FirstName = _value.FirstName,
                        IsRead = _value.IsRead,
                        LastName = _value.LastName,
                        PatientAccount = _value.PatientAccount,
                        RequestedDate = _value.RequestedDate,
                        RepliedDate = _value.RepliedDate,
                        RepliedMessage = _value.RepliedMessage,
                        ProviderId = EncryptDecrypt.Encrypt(_value.ProviderId)
                    });
                }
                return objRequestList;
            }
        }

        [HttpGet]
        public IEnumerable<PatientDemographicsStats> FetchPatientDemographicsStats(string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            List<PatientDemographicsStats> objRequestStats = new List<PatientDemographicsStats>();
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                var _returnvalue = context.Database.SqlQuery<PatientDemographicsStats>("Exec Medflow_PatientDemographicsRequestStats").ToList<PatientDemographicsStats>();
                foreach (var _value in _returnvalue)
                {
                    objRequestStats.Add(new PatientDemographicsStats()
                    {
                        NewRequest = _value.NewRequest,
                        PendingRequest = _value.PendingRequest,
                        CloseRequest = _value.CloseRequest
                    });
                }
                return objRequestStats;
            }
        }

        [HttpGet]
        public int ReadRequest_PatientDemographics(string RequestId, string ClientKey)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            int retStatus = 0;
            using (var context = new MedflowContext(DecryptedPracticeId))
            {
                try
                {
                    var Param_RequestId = new SqlParameter("@RequestId", RequestId);
                    context.Database.ExecuteSqlCommand("Update Medflow.PatientDemographicsRequest Set IsRead = '1' Where RequestId = @RequestId", Param_RequestId);
                    retStatus = 1;
                }
                catch (Exception ee)
                {
                    retStatus = 0;
                }
                return retStatus;
            }
        }

        [HttpGet]
        public async Task<int> SendPatientDemographicsResponse(string ClientKey, int PatientAccount, string ExternalId, string ReplyMessage, string RequestId, string ProviderId, string RequestedDate)
        {
            string DecryptedPracticeId = EncryptDecrypt.Decrypt(ClientKey);
            var business = new MedflowPortalBusiness();
            int rowsaffected = 0;

            string DecryptedProviderId = EncryptDecrypt.Decrypt(ProviderId);
            var DemographicsResponse = new PatientDemographicsResponse
            {
                RequestedDate = RequestedDate,
                PatientDetail = new Patient(PatientAccount.ToString(), DecryptedPracticeId),
                Subject = "Patient Demographics Change Request",
                ReplyMessage = ReplyMessage,
                ExternalId = ExternalId,
                ProviderId = ProviderId,
                Sender = "Provider",
                ResponseDate = DateTime.Now.ToString(),
            };

            var XML_Response = business.GenerateXmlforPatientDemographicsResponse(DemographicsResponse);

            using (var dbcontext = new MedflowContext(DecryptedPracticeId))
            {
                business.LoggerExceptions("SendPatientDemographicsResponse", "Inserting  into Medflow.PortalQueueXML Table");

                var insertportalqueuexmltable = string.Format("INSERT INTO Medflow.PortalQueueXML(MessageData, ActionTypeLookupId, ProcessedDate, PatientId, IsActive, ExternalId) " + "Values ('{0}',{1},'{2}',{3},{4},{5})", XML_Response, 2, DateTime.Now.ToString(), PatientAccount, 1, ExternalId);

                await Task.Run(() => dbcontext.Database.ExecuteSqlCommand(insertportalqueuexmltable));
                business.LoggerExceptions("SendPatientDemographicsResponse", "Insertion into Medflow.PortalQueueXML Table completed");
            }

            //string EncryptedData = AESEncryptDecrypt.Encrypt(XML_Response, DecryptedPracticeId); 

            var Parameter_to_Send = new
            {
                ClientKey = DecryptedPracticeId,
                ActionTypeLookupTypeId = 2,
                MessageData = XML_Response,
                ExternalId = ExternalId,
                Patientid = PatientAccount,
                ProviderId = DecryptedProviderId,
                ProcessedDate = DateTime.Now.ToString()
            };

            try
            {
                business.LoggerExceptions("SendPatientDemographicsResponse", "Parameters has been Created and now accessing Medflow's shared Api");
                string result = string.Empty;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(Parameter_to_Send);
                string medflowurl = ConfigurationManager.AppSettings["medflowApiUrl"].ToString();

                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;

                business.LoggerExceptions("SendPatientDemographicsResponse", "Posting Created Parameters to the Medflow API");

                //Posting Created Parameters to the Medflow API

                result = webclient.UploadString(medflowurl, "POST", json);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();

                business.LoggerExceptions("SendPatientDemographicsResponse", "Data has been uploaded on the Medflow Patient portal Database");

                if (result == "success")
                {
                    business.LoggerExceptions("SendPatientDemographicsResponse", "Success! Inserting into Medflow.PortalQueueXMLResponse Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLResponse(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, OriginalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", Parameter_to_Send.MessageData, 2, DateTime.Now, Parameter_to_Send.Patientid, 1, Parameter_to_Send.ExternalId);
                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }
                else
                {
                    business.LoggerExceptions("SendPatientDemographicsResponse", "Failed! Inserting into Medflow.PortalQueueXMLException Table");
                    using (var db = new MedflowContext(DecryptedPracticeId))
                    {
                        var query = String.Format("INSERT INTO medflow.PortalQueueXMLException(MessageData, ActionTypeLookUpId, ProcessedDate, PatientId, IsActive, ExternalId) VALUES ('{0}',{1},'{2}',{3},{4},{5})", Parameter_to_Send.MessageData, 2, DateTime.Now, Parameter_to_Send.Patientid, 1, Parameter_to_Send.ExternalId);
                        rowsaffected = await Task.Run(() => db.Database.ExecuteSqlCommand(query));
                    }
                }

                business.LoggerExceptions("SendPatientDemographicsResponse", "Updating Status into Medflow.PatientAmendmentInfo Table");
                using (var db = new MedflowContext(DecryptedPracticeId))
                {
                    var _updateQuery = "Update Medflow.PatientDemographicsRequest Set RepliedMessage = '" + ReplyMessage.Replace("'", "''") + "', RepliedDate = '" + DateTime.Now.ToString() + "' Where RequestId = '" + RequestId + "'";
                    await Task.Run(() => db.Database.ExecuteSqlCommand(_updateQuery));
                }
                business.LoggerExceptions("SendPatientDemographicsResponse", "Updating Status into Medflow.PatientAmendmentInfo Completed");
            }
            catch (Exception ex)
            {
                business.LoggerExceptions("SendPatientDemographicsResponse", "Error while inserting Medflow.PortalQueueXMl Table -" + ex.Message);
            }

            return rowsaffected;
        }
    }


}
