﻿using System.Web;
using System.Web.Optimization;

namespace PatientPortalDashBoard
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-1.12.4.js", "~/Scripts/jquery.blockUI.js"
                       )); // Added For jQuery Block UI 

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/customjs").Include(
                 "~/Scripts/sb-admin-2.js",
                 "~/Scripts/metisMenu.js",
                 "~/Scripts/jquery.limit-1.2.source.js",
                 "~/CustomJS/Common.js"
                ));

            //new Bundle added for Custom Js

            bundles.Add(new ScriptBundle("~/bundles/LoginJS").Include(
                "~/CustomJS/Login.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/IndexJS").Include(
               "~/CustomJS/Index.js"
               ));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.min.css", new CssRewriteUrlTransform()
                ));           

            bundles.Add(new StyleBundle("~/Content/customcss").Include(
                "~/Content/metisMenu.css",
                "~/Content/timeline.css",
                "~/Content/sb-admin-2.css",
                "~/Content/morris.css",
                "~/Content/Login.css"
                ).Include("~/IOSecureMessagingDashBoard/Content/font-awesome.min.css", new CssRewriteUrlTransform())); //~/Content/font-awesome.min.css

            //bundles.Add(new LessBundle("~/Content/less").Include(
            //    "~/Content/sb-admin-2.less",
            //    "~/Content/mixins.less",
            //    "~/Content/variables.less"
            //    ));

        }
    }
}
