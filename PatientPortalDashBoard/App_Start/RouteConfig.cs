﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PatientPortalDashBoard
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "ResetPassword",
               url: "Account/ResetPassword/{PracticeId}/{ProviderId}",
               defaults: new { controller = "Account", action = "ResetPassword", PracticeId = UrlParameter.Optional, ProviderId = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "ResetOTPDetail",
               url: "Account/ResetOTPDetail/",
               defaults: new { controller = "Account", action = "ResetOTPDetail", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "ForgotPassword",
               url: "Account/ForgotPassword/{PracticeId}",
               defaults: new { controller = "Account", action = "ForgotPassword", PracticeId = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "PracticeRegistration",
                url: "Account/PracticeRegistration/",
                defaults: new { controller = "Account", action = "PracticeRegistration", Id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Error",
                url: "Account/Error/",
                defaults: new { controller = "Account", action = "Error", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "Home/Index/",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Amendments",
                url: "Home/Amendments/",
                defaults: new { controller = "Home", action = "Amendments", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ProvidersList",
                url: "Home/ProvidersList/",
                defaults: new { controller = "Home", action = "ProvidersList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Appointments",
               url: "Home/Appointments/",
               defaults: new { controller = "Home", action = "Appointments", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "Medication",
               url: "Home/Medication/",
               defaults: new { controller = "Home", action = "Medication", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "CreateAuth",
               url: "Account/CreateAuth/",
               defaults: new { controller = "Account", action = "CreateAuth", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "Demographics",
               url: "Home/Demographics/",
               defaults: new { controller = "Home", action = "Demographics", id = UrlParameter.Optional }
           );


            routes.MapRoute(
                name: "Login1",
                url: "Account/Login/{PracticeId}",
                defaults: new { controller = "Account", action = "Login", PracticeId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Login2",
                url: "{PracticeId}",
                defaults: new { controller = "Account", action = "Login", PracticeId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "UserRegistration",
                url: "Account/UserRegistration",
                defaults: new { controller = "Account", action = "UserRegistration", PracticeId = UrlParameter.Optional }
            );
            
        }
    }
}
