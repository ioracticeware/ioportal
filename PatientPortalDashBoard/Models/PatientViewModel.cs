﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientPortalDashBoard.Models
{
    public class PatientViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int PatientAccount { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public int ProviderId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string ClientKey { get; set; }
       
    }

    public class PostPushtoPortal
    {
        public string ClientKey { get; set; }
        public int ActionTypeLookupTypeId { get; set; } //ActionTypeLookupTypeId
        public string MessageData { get; set; }
        public DateTime ProcessedDate { get; set; }
        public int ExternalId { get; set; }
        public int PatientId { get; set; }
        public int IsActive { get; set; }

    }
}