﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PatientPortalDashBoard.Models
{
    public class PracticeInformation
    {
        public string PracticeId = "";
        public string Practicekey = "";
        public string PracticeName = "";
        public string PracticeDisplayName = "";
        public string PracticeURL = "";
        public string Status = "";

        public PracticeInformation()
        {

        }

        public PracticeInformation(string PracticeId)
        {
            string _SelectQry = "Select * From dbo.PracticeInformation Where Id = '" + PracticeId + "'";
            using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConString"].ToString()))
            {
                Connection.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, Connection);
                SqlDataReader objReader = cmd.ExecuteReader();
                if (objReader.Read())
                {
                    this.PracticeId = objReader["Id"].ToString();
                    this.Practicekey = objReader["PracticeKey"].ToString();
                    this.PracticeName = objReader["PracticeName"].ToString();
                    this.PracticeDisplayName = objReader["PracticeDisplayName"].ToString();
                    this.PracticeURL = objReader["PracticeURL"].ToString();
                    this.Status = objReader["Status"].ToString();
                }
                else
                {
                    this.PracticeId = "";
                }
                objReader.Close();
                Connection.Close();
            }
        }

        public static List<PracticeInformation> GetALLPracticeInformation()
        {
            List<PracticeInformation> PraticeList = new List<PracticeInformation>();
            string _SelectQry = "Select * From dbo.PracticeInformation Order by PracticeName";
            using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConString"].ToString()))
            {
                Connection.Open();
                SqlCommand cmd = new SqlCommand(_SelectQry, Connection);
                SqlDataReader objReader = cmd.ExecuteReader();
                while (objReader.Read())
                {
                    PraticeList.Add(new PracticeInformation { PracticeDisplayName = objReader["PracticeName"].ToString(), Practicekey = objReader["Practicekey"].ToString() });
                }
                objReader.Close();
                Connection.Close();
            }
            return PraticeList;
        }
    }
}