﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientPortalDashBoard.Models
{
    public class Provider
    {
        public string Providerid { get; set; }
        public string DisplayName { get; set; }
        public string PracticeName { get; set; }
        public string PracticeId { get; set; }
        public string isAdmin { get; set; }
    }
}