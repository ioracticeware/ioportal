﻿

namespace PatientPortalDashBoard.Models
{
    public class UserProfileViewModel
    {
        public int? id { get; set; }
        public string practiceid { get; set; }
        public string displayname { get; set; }
        public string practicename { get; set; }
        public string isAdmin { get; set; }

        //id = @providerinfo.Providerid, practiceid = @providerinfo.PracticeId,displayname = @providerinfo.DisplayName ,isAdmin = @providerinfo.isAdmin,practicename = @providerinfo.PracticeName

    }
}