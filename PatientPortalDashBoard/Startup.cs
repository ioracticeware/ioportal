﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PatientPortalDashBoard.Startup))]
namespace PatientPortalDashBoard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
