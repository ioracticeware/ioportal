﻿
//divpatientlist - -- div id needs to be show on search click button


function binddynamictable(providerinfo,clientkey) {
    $.ajax({
        url: 'https://portal.iopracticeware.com/IOPWWebAPI/api/Medflow/FetchAllSecureMessages/?providerid=' + providerinfo + '&clientkey='+clientkey,
        type: 'GET',
        dataType: 'json',
        cache: false,
        crossdomain: true,
        //data: { 'providerid': providerinfo, 'clientkey': clientkey },
        success: function (data, textStatus, xhr) {
            if (data.d !== '') {

                $('#tbodysecuremessage').empty();
                $('#tblname').html('Inbox');

                $.each(data, function (index) {

                    // alert(JSON.stringify(data[index]));

                    var _newindex = index + 1;

                    if (data[index].isreplied == 'yes') {

                        var d = new Date(data[index].CreatedDate);
                        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                        var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                        var time = d.toLocaleTimeString().toLowerCase();

                        $("#tblMessages > tbody").append("<tr class='warning'><td>" + _newindex + "</td><td style='display:none'>" + data[index].Id + "</td><td>" + date + ' ' + "at" + ' ' + time + "</td><td>" + data[index].LastName + ',' + data[index].FirstName + "</td><td>" + data[index].Subject + "</td><td>" + data[index].Message + "</td></tr>");
                    }
                    else if (data[index].isreplied == 'no') {

                        var d = new Date(data[index].CreatedDate);
                        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                        var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                        var time = d.toLocaleTimeString().toLowerCase();

                        $("#tblMessages > tbody").append("<tr class='danger'><td>" + _newindex + "</td><td style='display:none'>" + data[index].Id + "</td><td>" + date + ' ' + "at" + ' ' + time + "</td><td>" + data[index].LastName + ',' + data[index].FirstName + "</td><td>" + data[index].Subject + "</td><td>" + data[index].Message + "</td></tr>");
                    }

                });

                $.unblockUI();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('some error occurred while binding the Table dynamically');
        }
    });
}

$("#tblMessages > tbody").delegate('tr', 'click', function () {

    var portalinboxid = $(this).closest('tr').find('td:eq(1)').html();
    var _subject = $(this).closest('tr').find('td:eq(4)').html();

    $('#hdnsubject').val(_subject);

    $('#hdnportalid').val(portalinboxid);

    //$.blockUI({ message: '<h4> <img src="@Url.Content("~/images/728.gif")" />Showing Chat...</h4>' });

    $.blockUI({ message: '<h4> <img src=" '+ domain +' /images/728.gif" />Please wait...</h4>' });

    $.ajax({
        url: 'http://192.168.22.22:9090/api/Medflow/FetchChatHistory/?portalinboxid=' + portalinboxid, //localhost:62845  192.168.22.22:9090
        type: 'GET',
        dataType: 'json',
        cache: false,
        crossdomain: true,
       // data: portalinboxid,
        success: function (data, textStatus, xhr) {

            if (data[0].ProviderReply !== null) {

                var bindhtml = "<ul class='chat'>";
                $.each(data, function (index) {


                    // alert(JSON.stringify(data[index]));

                    bindhtml += "<li class='left clearfix'>";
                    bindhtml += "<span class='chat-img pull-left'>";
                    bindhtml += "<img src=' " + domain + " /images/patient_image.png'' alt='Patient Image' class='img-circle' />";
                    bindhtml += "</span>";
                    bindhtml += "<div class='chat-body clearfix'>";
                    bindhtml += "<div class='header'>";
                    bindhtml += "<strong class='primary-font'>Patient</strong>";
                    bindhtml += "</div>";
                    bindhtml += '<input type="text" class="form-control input-lg" style="width:400px;overflow-y:scroll;" name="pttext' + data[index] + '" id="pttext' + data[index] + '" value="' + data[index].PatientMessage + '" disabled>';
                    bindhtml += "</li>";
                    bindhtml += "<li class='right clearfix'>";
                    bindhtml += "<span class='chat-img pull-right'>";
                    // bindhtml += "<img src='@Url.Content(@"~/images/doctorimage.jpg")' alt='Provider Image' class='img-circle' />";
                    bindhtml += "<img src='"+ domain +"/images/doctorimage.jpg'' alt='Provider Image' class='img-circle' />";

                    bindhtml += "</span>";
                    bindhtml += "<div class='chat-body clearfix'>";
                    bindhtml += "<div class='header'>";
                    bindhtml += "<strong class='pull-right primary-font'>Provider</strong>";
                    bindhtml += "</div>";
                    //bindhtml += '<textarea class="form-control custom-control" rows="3" style="resize:none" id="providertext'+ index +' " disabled></textarea>'
                    bindhtml += '<input type="text" class="form-control input-lg" style="width:400px;overflow-y:scroll;height:50px;" name="providertext' + data[index] + '" id="providertext' + data[index] + '" value="' + data[index].ProviderReply + '" disabled>';

                    bindhtml += "</li>";


                });

                bindhtml += "</ul>";

                var _isclosedflag = data[0].isClosed;


                var subject = $('#hdnsubject').val();


                showChatModal(subject, bindhtml, _isclosedflag);


                $.unblockUI();

            }
                //in case there's no chained message
            else {
                var bindhtmlforreply = "<ul class='chat'>";
                $.each(data, function (index) {

                    //alert(data[index].Subject);

                    var d = new Date(data[index].CreatedDate);
                    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                    var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                    var time = d.toLocaleTimeString().toLowerCase();



                    bindhtmlforreply += "<li class='left clearfix'>";
                    bindhtmlforreply += "<span class='chat-img pull-left'>";
                    bindhtmlforreply += "<strong class='primary-font'>Subject : </strong>";
                    bindhtmlforreply += "</span>";
                    bindhtmlforreply += "<div class='form-group'>";
                    bindhtmlforreply += "<div class='header'>";
                    bindhtmlforreply += "<p>" + data[index].Subject + "</p>";
                    bindhtmlforreply += "</div>";
                    bindhtmlforreply += "</li>";
                    bindhtmlforreply += "<li class='right clearfix'>";
                    bindhtmlforreply += "<div class='form-group'>";
                    bindhtmlforreply += "<div class='header'>";
                    bindhtmlforreply += "<strong class='pull-right primary-font'>" + data[index].LastName + ' ' + data[index].FirstName + '&#64;' + ' ' + date + ' ' + ' ' + time + "</strong>";
                    bindhtmlforreply += "</div>";
                    bindhtmlforreply += "<p>" + data[index].Message + "</p>";
                    bindhtmlforreply += "</li>";

                });
                bindhtmlforreply += "</ul>";

                var _isclosed = data[0].isClosed;

                showChatModal(subject, bindhtmlforreply, _isclosedflag);

                $.unblockUI();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Error occurred while showing thread messages from Table');
        }
    });


});


//Function for fetching the Message count
function fetchmessagecount(providerid,clientkey) {

    $.ajax({
        url: 'http://192.168.22.22:9090/api/Medflow/FetchMessageCount', //?providerid=' + providerid
        type: 'GET',
        dataType: 'json',
        cache: false,
        crossdomain: true,
        data: { 'providerid': providerid, 'clientkey': clientkey },
        success: function (data, textStatus, xhr) {
            if (data.d !== '') {

                $('#dvactive').html(data[0].activemessagecount);
                $('#dvpending').html(data[0].pendingmessagecount);
                $('#dvclosed').html(data[0].closedmessagecount);

            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Error in Fetching message count for logged in provider');
        }
    });
}


function showChatModal(title, html, isclosed) {


    if (isclosed == 'Y') {
        $("#chatmodal .modal-title").html(title);
        $("#chatmodal .modal-body").html(html);
        $('#btnclose').hide();
        $('#btnreply').hide();
        $('#chatmodal').modal("show");

    }
    else {
        $("#chatmodal .modal-title").html(title);
        $("#chatmodal .modal-body").html(html);
        $('#btnclose').show();
        $('#btnreply').show();
        $('#chatmodal').modal("show");
    }
}




function ComposeMessageModal(title, html) {
    $("#modalCompose .modal-title").html(title);
    var _subject = $('#hdnsubject').val();

    $('#inputSubject').val(_subject);

    $('#modalCompose').modal("show");
    $('txtmessage').focus();
}



$('#btnreply').on('click', function () {
    $('#chatmodal').modal('hide');
    ComposeMessageModal('Reply');
});

$('#btnclose').on('click', function () {
    $('#chatmodal').modal('hide');
    var _portalid = $('#hdnportalid').val();
    closemessagethread(_portalid);
});




//$('#btnsendreply').on('click', function () {
//    //  var portalid = $('#hdnportalid').val();
//    //  var description = $('#txtmessage').val();

//    // SendReply(portalid, description);
//});





//funtion for sending the reply to a Message from Patient
function SendReply(portalid, description) {


    if (description == '') {
        $('#modalCompose').modal('hide');
        Showsuccessmessage('Alert!', 'Please fill the message field');

    }
    else {

        $.ajax({
            url: 'http://192.168.22.22:9090/api/Medflow/SendReplytoPatient', ///?portalinboxid=' + portalid + '&Description=' + description
            type: 'POST',
            dataType: 'json',
            cache: false,
            crossdomain: true,
            data: { 'portalinboxid': portalid, 'Description': description },
            success: function (data, textStatus, xhr) {
                if (data[0].status !== '') {
                    $('#hdnportalid').val('');
                    $('#hdnsubject').val('');
                    $('#txtmessage').val('');
                    $('#modalCompose modal-body').html('');
                    $('#modalCompose').modal('hide');

                    Showsuccessmessage('Reply', 'Reply has been sent to the patient');

                    binddynamictable(parseInt(@providerinfo.Providerid));
                    fetchmessagecount(parseInt(@providerinfo.Providerid))
                }
                else {
                    alert('data not returned');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert('some error occurred in Send Reply Function');
            }
        });
    }
}


//funtion for closing the message thread
function closemessagethread(portalid) {

    $.ajax({
        url: 'http://192.168.22.22:9090/api/Medflow/closemessagethread/?portalinboxid=' + portalid,
        type: 'POST',
        dataType: 'json',
        cache: false,
        crossdomain: true,
        data: portalid,
        success: function (data, textStatus, xhr) {
            if (data[0].closed == 'closed') {
                $('#hdnportalid').val('');
                $('#hdnsubject').val('');

                Showsuccessmessage('Message Closed', 'Message thread has been closed');

                binddynamictable(parseInt(@providerinfo.Providerid));
                fetchmessagecount(parseInt(@providerinfo.Providerid))
            }
            else {
                alert('data not returned');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('some error occurred in Send Reply Function');
        }
    });
}



function Showsuccessmessage(title, html) {
    $("#successmodal .modal-title").html(title);
    $("#successmodal .modal-body").html(html);
    $('#successmodal').modal("show");
}



$('.btn-group').on('click', function () {
    $(this).removeClass('btn-group');
    $(this).addClass('btn-group open');
});

$('.dropdown').on('click', function () {
    $(this).removeClass('dropdown');
    $(this).addClass('dropdown open');
});




//function for Fetching secure messages from DB based on the provided status

function fetchrecordsbasedonstatus(statusid) {
    var provideruserid = parseInt(@providerinfo.Providerid);


    //$.blockUI({ message: '<h4> <img src="@Url.Content("~/images/728.gif")" />Please wait...</h4>' });

    $.blockUI({ message: '<h4> <img src=" '+ domain +' /images/728.gif" />Please wait...</h4>' });

    $.ajax({
        url: 'http://192.168.22.22:9090/api/Medflow/fetchrecordsbasedonstatus/?typeid=' + statusid + '&providerid=' + provideruserid,
        type: 'POST',
        dataType: 'json',
        cache: false,
        crossdomain: true,
        data: { 'typeid': statusid, 'providerid': provideruserid },//'typeid=' + status + '&providerid=' + providerid,//{ 'typeid': status, 'providerid': providerid }, //'foo='+ bar+'&calibri='+ nolibri
        success: function (data, textStatus, xhr) {
            if (data.d !== '') {

                if (statusid == '1') {

                    $('#tbodysecuremessage').empty();
                    $('#tblname').html('Pending Messages');
                    $.each(data, function (index) {

                        var _newindex = index + 1;


                        var d = new Date(data[index].CreatedDate);
                        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                        var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                        var time = d.toLocaleTimeString().toLowerCase();

                        $("#tblMessages > tbody").append("<tr class='info'><td>" + _newindex + "</td><td style='display:none'>" + data[index].Id + "</td><td>" + date + ' ' + "at" + ' ' + time + "</td><td>" + data[index].LastName + ',' + data[index].FirstName + "</td><td>" + data[index].Subject + "</td><td>" + data[index].Message + "</td></tr>");
                    });

                    $.unblockUI();
                }

                else if (statusid == '2') {
                    //  alert('new');

                    $('#tbodysecuremessage').empty();
                    $('#tblname').html('New Messages');
                    $.each(data, function (index) {
                        var _newindex = index + 1;
                        var d = new Date(data[index].CreatedDate);
                        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                        var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                        var time = d.toLocaleTimeString().toLowerCase();

                        $("#tblMessages > tbody").append("<tr class='danger'><td>" + _newindex + "</td><td style='display:none'>" + data[index].Id + "</td><td>" + date + ' ' + "at" + ' ' + time + "</td><td>" + data[index].LastName + ',' + data[index].FirstName + "</td><td>" + data[index].Subject + "</td><td>" + data[index].Message + "</td></tr>");
                    });

                    $.unblockUI();
                }

                else {
                    $('#tbodysecuremessage').empty();
                    $('#tblname').html('Closed Messages');
                    $.each(data, function (index) {
                        var _newindex = index + 1;

                        var _newindex = index + 1;
                        var d = new Date(data[index].CreatedDate);
                        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                        var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                        var time = d.toLocaleTimeString().toLowerCase();

                        $("#tblMessages > tbody").append("<tr class='warning'><td>" + _newindex + "</td><td style='display:none'>" + data[index].Id + "</td><td>" + date + ' ' + "at" + ' ' + time + "</td><td>" + data[index].LastName + ',' + data[index].FirstName + "</td><td>" + data[index].Subject + "</td><td>" + data[index].Message + "</td></tr>");
                    });

                    //warning
                    $.unblockUI();
                }

                $.unblockUI();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('some error occurred while binding the Table based on status');
        }
    });

}


function bindallusers() {
    var _currentprovider = parseInt(@providerinfo.Providerid);
    var _practiceid = '@providerinfo.PracticeId';

    $.ajax({
        url: 'http://192.168.22.22:9090/api/Medflow/fetchalltherestproviders/?providerid=' + _currentprovider + "&practiceid=" + _practiceid,
        type: 'GET',
        dataType: 'json',
        cache: false,
        crossdomain: true,
        data: _currentprovider,
        success: function (data, textStatus, xhr) {
            if (data.d !== '') {

                var _bindelement = '';
                $.each(data, function (index) {

                    _bindelement += "<li>";
                    _bindelement += "<a href='#' id=" + data[index].id + ">" + data[index].displayname + "</a>";
                    _bindelement += "</li>";

                });




                $('#ul_menu').append(_bindelement);


            }
        },
        error: function (xhr, textStatus, errorThrown) {

            alert('Error while binding the Other provider information on side menu');
        }
    });


}


//binding click events on Dynamically added controls
$('#ul_menu').on('click', 'a', function (event) {


    //alert(this.id);
    var providerid = this.id;
    binddynamictable(providerid);
    event.preventDefault();
});


//new Message count
$('#spactive').on('click', function () {
    fetchrecordsbasedonstatus('2');
});

//pending Message count
$('#sppending').on("click", function () {
    fetchrecordsbasedonstatus('1');
});

//closed Message count
$('#spclosed').on("click", function () {
    fetchrecordsbasedonstatus('3');
});

//new icon
$('#iconnew').on('click', function () {
    fetchrecordsbasedonstatus('2');
});

//pending icon
$('#iconpending').on("click", function () {
    fetchrecordsbasedonstatus('1');
});

//closed icon
$('#iconclosed').on("click", function () {
    fetchrecordsbasedonstatus('3');
});


$('#btncancel').click(function () {
    //Reset Message Text
    $('#txtmessage').val('');
});


$('#btncross').click(function () {
    //reset message Text
    $('#txtmessage').val('');
});

$('#txtmessage').limit('500', '#charsLeft');
// $('#txtmessage2').limit('200', '#charsLeft2');
//Delegating Click event to SEarch Button on click

$(document).delegate("#btnSearch", "click", function (event) {

    $('#divpatientlist').show();

    var _lastname = $('#txtlastname').val();

    $('#tbodyPatientList').empty();

    $.ajax({
        url: 'http://localhost:62845/api/Medflow/FetchPatients/?LastName=' + _lastname,
        type: 'POST',
        dataType: 'json',
        cache: false,
        crossdomain: true,
        success: function (data, textStatus, xhr) {
            if (data.d !== '') {

                $.each(data, function (index) {
                    // alert(JSON.stringify(data[index]));
                    var _newindex = index + 1;

                    var d = new Date(data[index].DOB);
                    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                    var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                    var time = d.toLocaleTimeString().toLowerCase();

                    $("#tblPatientList > tbody").append("<tr class='warning'><td>" + data[index].LastName + "</td><td style='display:none'>" + data[index].PatientAccount + "</td><td>" + data[index].FirstName + "</td><td>" + data[index].MiddleName + "</td><td>" + date + "</td><td>" + data[index].Gender + "</td></tr>");

                });

                $('#txtlastname').val('');
                $('#btnSearch').focusout();

            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Error in Fetching message count for logged in provider');
        }
    });

});


$("body").on("click", "#tblPatientList tbody tr", function () {


    var _patientId = $(this).closest('tr').find('td:eq(1)').html();
    var _firstName = $(this).closest('tr').find('td:eq(2)').html();
    var _lastName = $(this).closest('tr').find('td:eq(0)').html();
    var _middleName = $(this).closest('tr').find('td:eq(3)').html();
    var _dob = $(this).closest('tr').find('td:eq(4)').html();
    var _gender = $(this).closest('tr').find('td:eq(5)').html();

    $('#divpatientlist').hide();
    $('#txtlastname').hide();
    $('#btnSearch').hide();

    $('#second').show();

    var _subject = $('#inputSubject2').val();
    var _messagebody = $('#txtmessage2').val();

    $("#hdnpatientid").val(_patientId);
    $("#hdnfirstname").val(_firstName);
    $("#hdnlastname").val(_lastName);
    $("#hdnmiddlename").val(_middleName);
    $("#hdndob").val(_dob);
    $("#hdngender").val(_gender);


});



// Click event for new send button.. ** in case of new message by Provider ***

$(document).delegate("#btnSendnew", "click", function (event) {

    var _subject = $('#text_newmessage1').val();
    var _message = $('#text_newmessage2').val();
            
    var _providerId = parseInt(@providerinfo.Providerid);
    $.post("/Home/CreatePatientData", { FirstName: $("#hdnfirstname").val(), LastName: $("#hdnlastname").val(), MiddleName: $("#hdnmiddlename").val(), PatientAccount: $("#hdnpatientid").val(), DOB: $("#hdndob").val(), Gender: $("#hdngender").val(), ProviderId: _providerId, Subject: _subject, Message: _message },
      function (data) {

          $("#hdnpatientid").val('');
          $("#hdnfirstname").val('');
          $("#hdnlastname").val('');
          $("#hdnmiddlename").val('');
          $("#hdndob").val('');
          $("#hdngender").val('');


          $('#newmessagemodal').modal('hide');

          //  alert(data);

          Showsuccessmessage('Sccess', 'New Message From Provider has been sent to Patient'); 
                  
      });

});