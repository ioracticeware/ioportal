﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Net.Http;
using PatientPortalDashBoard.Models;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using System.Web.Security;

namespace PatientPortalDashBoard.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Login(string PracticeId)
        {
            FormsAuthentication.SignOut();
            if (PracticeId != null)
            {
                PracticeInformation objPractice = new PracticeInformation(PracticeId);
                if (objPractice.PracticeId == PracticeId)
                {
                    ViewBag.PracticeId = PracticeId;
                    ViewBag.PracticeName = objPractice.PracticeDisplayName;
                    return View();
                }
                else
                {
                    return RedirectToAction("Error", "Account");
                }
            }
            else
            {
                return RedirectToAction("Error", "Account");
            }
        }

        public ActionResult ForgotPassword(string PracticeId)
        {
            if (PracticeId != null)
            {
                PracticeInformation objPractice = new PracticeInformation(PracticeId);
                if (objPractice.PracticeId == PracticeId)
                {
                    ViewBag.PracticeId = PracticeId;
                    ViewBag.PracticeName = objPractice.PracticeDisplayName;
                    return View();
                }
                else
                {
                    return RedirectToAction("Error", "Account");
                }
            }
            else
            {
                return RedirectToAction("Error", "Account");
            }
        }

        public ActionResult PracticeRegistration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PracticeRegistration(FormCollection FC)
        {
            string PracticeKey = FC["PracticeKey"].ToString();
            string PracticeName = FC["PracticeName"].ToString().Replace("'","''");
            string PracticeDisplayName = FC["PracticeDName"].ToString().Replace("'", "''");

            if (PracticeKey.Trim() != "" && PracticeName.Trim() != "" && PracticeDisplayName.Trim() != "")
            {
                try
                {
                    string EncryptedKey = Encrypt.Encryption(PracticeKey);
                    string Qry = "Insert into dbo.PracticeInformation(Id, PracticeKey, PracticeName, PracticeDisplayName, PracticeURL, Status) Values ('" + EncryptedKey + "', '" + PracticeKey + "', '" + PracticeName + "', '" + PracticeDisplayName + "', 'NA', 1)";

                    using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConString"].ToString()))
                    {
                        con.Open();
                        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(Qry, con);
                        cmd.ExecuteNonQuery();

                    }
                }
                catch(Exception ee)
                {
                    
                }
            }
 

            return View();
        }

        public ActionResult UserRegistration()
        {
            ViewBag.PracticeList = PracticeInformation.GetALLPracticeInformation();
            return View();
        }

        public ActionResult ResetOTPDetail(string providerid, string displayName, string practicename, string practiceid, string isAdmin)
        {
            if (practiceid != null)
            {
                PracticeInformation objPractice = new PracticeInformation(practiceid);
                if (objPractice.PracticeId == practiceid)
                {
                    ViewBag.PracticeId = practiceid;
                    ViewBag.PracticeName = objPractice.PracticeDisplayName;
                    ViewBag.ProviderId = providerid;
                    ViewBag.DisplayName = displayName;
                    ViewBag.isAdmin = isAdmin;
                    return View();
                }
                else
                {
                    return RedirectToAction("Error", "Account");
                }
            }
            else
            {
                return RedirectToAction("Error", "Account");
            }
        }

        public ActionResult ResetPassword(string ProviderId,  string PracticeId)
        {
            if (PracticeId != null)
            {
                PracticeInformation objPractice = new PracticeInformation(PracticeId);
                if (objPractice.PracticeId == PracticeId)
                {
                    ViewBag.PracticeId = PracticeId;
                    ViewBag.PracticeName = objPractice.PracticeDisplayName;
                    ViewBag.ProviderId = ProviderId;
                    return View();
                }
                else
                {
                    return RedirectToAction("Error", "Account");
                }
            }
            else
            {
                return RedirectToAction("Error", "Account");
            }
        }

        public ActionResult CreateAuth(string providerid, string practiceid, string displayName, string practiceName, string isAdmin)
        {
            string UserDetail = providerid + "~" + practiceid;
            FormsAuthentication.SetAuthCookie(UserDetail, true);
            return RedirectToAction("Index", "Home", new { providerid = providerid, practiceid = practiceid, displayName = displayName, practiceName = practiceName, isAdmin = isAdmin });
        }

    }
}