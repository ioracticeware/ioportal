﻿
using System.Web.Mvc;
using System.Xml.Schema;
using System.Xml;
using PatientPortalDashBoard.Models;
using System;
using System.Net.Http;
using System.Net;
using System.Data;
using System.Configuration;
namespace PatientPortalDashBoard.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index(string providerid, string practiceid, string displayName, string practiceName, string isAdmin)
        {
            var providerinfo = new Provider
            {
                Providerid = providerid,
                DisplayName = displayName,
                PracticeName = practiceName,
                PracticeId = practiceid,
                isAdmin = isAdmin
            };
            ViewData["providerinfo"] = providerinfo;
            return View();
        }

        public ActionResult Amendments(string providerid, string practiceid, string displayName, string practiceName, string isAdmin)
        {
            var providerinfo = new Provider
            {
                Providerid = providerid,
                DisplayName = displayName,
                PracticeName = practiceName,
                PracticeId = practiceid,
                isAdmin = isAdmin
            };
            ViewData["providerinfo"] = providerinfo;
            return View();
        }

        public ActionResult Demographics(string providerid, string practiceid, string displayName, string practiceName, string isAdmin)
        {
            var providerinfo = new Provider
            {
                Providerid = providerid,
                DisplayName = displayName,
                PracticeName = practiceName,
                PracticeId = practiceid,
                isAdmin = isAdmin
            };
            ViewData["providerinfo"] = providerinfo;
            return View();
        }

        public ActionResult ProvidersList(string providerid, string practiceid, string displayName, string practiceName, string isAdmin)
        {
            var providerinfo = new Provider
            {
                Providerid = providerid,
                DisplayName = displayName,
                PracticeName = practiceName,
                PracticeId = practiceid,
                isAdmin = isAdmin
            };
            ViewData["providerinfo"] = providerinfo;
            return View();
        }

        public ActionResult Medication(string providerid, string practiceid, string displayName, string practiceName, string isAdmin)
        {
            var providerinfo = new Provider
            {
                Providerid = providerid,
                DisplayName = displayName,
                PracticeName = practiceName,
                PracticeId = practiceid,
                isAdmin = isAdmin
            };
            ViewData["providerinfo"] = providerinfo;
            return View();
        }


        // new action method for returning UserProfile View

        public ActionResult UserProfile(int? id, string practiceid, string displayname, string practicename, string isAdmin)
        {
            var _userprofileviewmodel = new UserProfileViewModel();

            _userprofileviewmodel.id = id;
            _userprofileviewmodel.practiceid = practiceid;
            _userprofileviewmodel.displayname = displayname;
            _userprofileviewmodel.practicename = practicename;
            _userprofileviewmodel.isAdmin = isAdmin;
            return View("UserProfile", _userprofileviewmodel);
        }

        //DeliverDoctorResponse
        public string GenerateXmlforDeliverDoctorResponse(PatientViewModel model)
        {
            string xml = string.Empty;

            string xsi = "http://www.w3.org/2001/XMLSchema-instance";
            string xsd = "http://www.w3.org/2001/XMLSchema";

            XmlDocument xmlDoc = new XmlDocument();
            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

            XmlElement documentElement = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            XmlSchema schema = new XmlSchema();
            schema.Namespaces.Add("xsi", xsi);
            schema.Namespaces.Add("xsd", xsd);
            xmlDoc.Schemas.Add(schema);


            XmlElement schemaNode = xmlDoc.CreateElement("Xml");
            schemaNode.SetAttribute("xmlns:xsi", xsi);
            schemaNode.SetAttribute("xmlns:xsd", xsd);
            xmlDoc.AppendChild(schemaNode);

            XmlNode rootNode = xmlDoc.CreateElement("Type");
            rootNode.InnerText = "DeliverDoctorResponse";
            schemaNode.AppendChild(rootNode);

            /*** Patient Part Starts here ***/

            XmlNode patient = xmlDoc.CreateElement("Patient");
            schemaNode.AppendChild(patient);

            XmlNode firstName = xmlDoc.CreateElement("FirstName");
            firstName.InnerText = Convert.ToString(model.FirstName);
            patient.AppendChild(firstName);

            XmlNode lastName = xmlDoc.CreateElement("LastName");
            lastName.InnerText = Convert.ToString(model.LastName);
            patient.AppendChild(lastName);


            XmlNode middleName = xmlDoc.CreateElement("MiddleName");
            middleName.InnerText = Convert.ToString(model.MiddleName);
            patient.AppendChild(middleName);


            XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
            patientAccount.InnerText = Convert.ToString(model.PatientAccount);
            patient.AppendChild(patientAccount);


            XmlNode dob = xmlDoc.CreateElement("DOB");
            dob.InnerText = Convert.ToString(model.DOB.ToString("MM/dd/yyyy"));
            patient.AppendChild(dob);

            XmlNode gender = xmlDoc.CreateElement("Gender");
            gender.InnerText = Convert.ToString(model.Gender);
            patient.AppendChild(gender);

            /***Patient part ends here*****/



            XmlNode securemessage = xmlDoc.CreateElement("SecureMessage");
            schemaNode.AppendChild(securemessage);


            // Binding Elements to  Child nodes
            XmlNode date = xmlDoc.CreateElement("RequestedDate");
            date.InnerText = Convert.ToString(System.DateTime.Now.ToString("MM/dd/yyyy"));
            securemessage.AppendChild(date);

            XmlNode Sender = xmlDoc.CreateElement("Sender");
            Sender.InnerText = Convert.ToString(model.PatientAccount);
            securemessage.AppendChild(Sender);

            XmlNode message = xmlDoc.CreateElement("Message");
            message.InnerText = Convert.ToString(model.Message);
            securemessage.AppendChild(message);

            XmlNode PortalMessageId = xmlDoc.CreateElement("PortalMessageId");
            PortalMessageId.InnerText = Convert.ToString("0");
            securemessage.AppendChild(PortalMessageId);

            XmlNode Subject = xmlDoc.CreateElement("Subject");
            Subject.InnerText = Convert.ToString(model.Subject);
            securemessage.AppendChild(Subject);

            XmlNode EMRMessageId = xmlDoc.CreateElement("EMRMessageId");
            EMRMessageId.InnerText = Convert.ToString("0");
            securemessage.AppendChild(EMRMessageId);

            XmlNode ProviderId = xmlDoc.CreateElement("ProviderId");
            ProviderId.InnerText = Convert.ToString(model.ProviderId);
            securemessage.AppendChild(ProviderId);

            xml = xmlDoc.InnerXml;

            return xml;
        }


        [HttpPost]
        public JsonResult CreatePatientData(PatientViewModel model)
        {
            string messagedata = GenerateXmlforDeliverDoctorResponse(model);

            var parametertosend = new PostPushtoPortal
            {
                ActionTypeLookupTypeId = 2,
                ClientKey = model.ClientKey,
                ProcessedDate = System.DateTime.Now,
                PatientId = model.PatientAccount,
                ExternalId = 4,
                MessageData = messagedata,
                IsActive = 1

            };

            string result = string.Empty;
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(parametertosend);
            string medflowurl = ConfigurationManager.AppSettings["medflowApiUrl"].ToString();

            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;

            result = webclient.UploadString(medflowurl, "POST", json);
            result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();

            if (result == "success")
            {
                //insert into the Medflow.portalQueueXMLTable
            }


            // Insert into Medflow.PortalQueueXML

            return Json(String.Format("'Success':'{0}'", result));

            //string success = "Success";
            //return Json(String.Format("'Success':'{0}'", success));

        }

    }
}